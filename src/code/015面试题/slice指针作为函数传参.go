// @Author: cyl
// @File: slice指针作为函数传参.go
// @Time: 2023/07/18 22:49:09
package main

import (
	"fmt"
)

func changeSlice(arr *[]int) {
	// len和cap要变，需要传切片指针
	*arr = append(*arr, 9) // arr=[1, 2, 3, 9] cap=6
	// len、pointer和cap要变，需要传切片指针
	*arr = (*arr)[1:2] // arr=[2] cap=5
	// slice结构体里的三个Field都不变，只改变底层数组，不需要传递切片指针
	(*arr)[0] = 9 // arr=[9] len=1, cap=5
}

func testChangeSlice() {
	arr := []int{1, 2, 3}
	fmt.Printf("len: %d, cap: %d, array: %v\n", len(arr), cap(arr), arr) // len: 3, cap: 3, array: [1 2 3]
	changeSlice(&arr)
	fmt.Printf("len: %d, cap: %d, array: %v\n", len(arr), cap(arr), arr) // len: 1, cap: 5, array: [9]
}

func main3() {
	testChangeSlice()
}
