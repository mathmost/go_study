### 1、append的性能损耗

1.1 切片是什么

![image-20230718075156978](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230718075156978.png)

因为array、len, cap都为int类型（int类型占8个字节），所以不管slice有多大，永远都只占24个字节 

1.2 append操作

![image-20230718075741698](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230718075741698.png)

1.3 append扩容系数

![image-20230718080054854](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230718080054854.png)

```go
package main

import (
	"fmt"
	"time"
)

// 探究capacity扩容规律
func expansion() {
	s := make([]int, 0, 3)
	prevCap := cap(s)
	for i := 0; i < 100; i++ {
		s = append(s, i)
		currCap := cap(s)
		if currCap > prevCap {
			// 每次扩容都是原先的2倍
			fmt.Printf("capacity从%d变成%d\n", prevCap, currCap)
			prevCap = currCap
		}
	}
	fmt.Println("==================")
}

const (
	LOOP = 1000000
)

func huge_append1() {
	begin := time.Now()
	arr := []int{}
	for i := 0; i < LOOP; i++ {
		arr = append(arr, i)
	}
	fmt.Printf("huge _append1 use time=%dms, arr len=%d\n", time.Since(begin).Milliseconds(), len(arr))
}

func huge_append2() {
	begin := time.Now()
	arr := make([]int, 0, LOOP)
	for i := 0; i < LOOP; i++ {
		arr = append(arr, i)
	}
	fmt.Printf("huge _append2 use time=%dms, arr len=%d\n", time.Since(begin).Milliseconds(), len(arr))
}

func SliceExpansionMain() {
	expansion()
}

func SliceHuge1AppendMain() {
	huge_append1()
}

func SliceHuge2AppendMain() {
	huge_append1()
}
```

运行SliceExpansionMain()，得出以下内容

```txt
capacity从3变成了6
capacity从6变成了12
capacity从12变成了24
capacity从24变成了48
capacity从48变成了96
capacity从96变成了192
```

运行SliceHuge1AppendMain()，得出以下内容

```txt
huge _append1 use time=18ms, arr len=1000000
```

运行SliceHuge2AppendMain()，得出以下内容

```txt
huge _append2 use time=3ms, arr len=1000000
```

### 2、母子切片内存共享问题

截取子切片

![image-20230718215813151](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230718215813151.png)

从1开始，创建一个切片arr, 再到对arr进行切片和赋值操作，当子切片的长度超过母切片，字母切片的内存会进行分离

![image-20230718220612515](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230718220612515.png)

代码验证一下

```go
package main

import "fmt"

func main() {
	s := make([]int, 3, 5)
	for i := 0; i < 3; i++ {
		s[i] = i + 1
	}
	fmt.Println("s:", s)
	fmt.Printf("s[1] address: %p\n", &s[1])

	subSlice := s[0:2]
	fmt.Printf("sub slice len:%d, cap:%d\n", len(subSlice), cap(subSlice)) 

	/** 母切片的内存使用完后，子切片再执行append就会申请一块新的内存地址，把老数据拷贝过来，在新内存上执行append操作 */
	subSlice = append(subSlice, 10)
	fmt.Printf("s=%v, subSlice=%v, s[1] address:%p, subSlice[1] address:%p\n", s, subSlice, &s[1], &subSlice[1])

	subSlice = append(subSlice, 6, 7)
	subSlice[0] = 8
	fmt.Printf("s=%v, subSlice=%v, s[1] address:%p, subSlice[1] address:%p\n", s, subSlice, &s[1], &subSlice[1])

	// 验证开辟新的内存后，s和subSlice地址是否不一致
	subSlice = append(subSlice, 100)
	fmt.Printf("s=%v, subSlice=%v, s[1] address:%p, subSlice[1] address:%p\n", s, subSlice, &s[1], &subSlice[1])
}
```

运行代码

```txt
// s = [1, 2, 3]
// s[1] address: 0xc0000b6038
// sub slice len:2, cap:5
// s=[1 2 10], subSlice=[1 2 10], s[1] address:0xc0000b6038, subSlice[1] address:0xc0000b6038
// s=[8 2 10], subSlice=[8 2 10 6 7], s[1] address:0xc0000b6038, subSlice[1] address:0xc0000b6038
// s=[8 2 10], subSlice=[8 2 10 6 7 100], s[1] address:0xc00019a038, subSlice[1] address:0xc0001a4008
```

### 3、切片导致的内存泄漏

![image-20230718224427027](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230718224427027.png)

```go
// @Author: cyl
// @File: slice_memory内存泄漏.go
// @Time: 2023/07/18 22:31:08
package main

import (
	"fmt"
)

const (
	TOTAL = 1 << 20
	begin = 2
	end   = 7
)

func return_sub_slice1() []int {
	parent := make([]int, TOTAL) // parent切片持有一个1M的int数组，占有内存8M
	child := parent[begin:end]   // 只要child没有被GC回收，长度为1M的int数组就一直得不到释放
	return child                 // child仍然持有底层的那个大数组
}

// 这个是解决方案
func return_sub_slice2() []int {
	parent := make([]int, TOTAL)
	length := end - begin          // 先计算出child切片的长度
	child := make([]int, length)   // 根据chile切片长度单独申请一个内存
	for i := begin; i < end; i++ { // 将child的值逐个拷贝到parent中
		child[i-begin] = parent[i]
	}
	return child
}

func use_sub_slice() {
	s1 := return_sub_slice1()
	fmt.Printf("s1 -> init len: %d, cap: %d, TOTAL: %d\n", len(s1), cap(s1), TOTAL)
	for i := 0; i < 5; i++ {
		s1 = append(s1, 9)
		fmt.Printf("s1 -> update len: %d, cap: %d\n", len(s1), cap(s1))
	}

	fmt.Println("========================================")

	s2 := return_sub_slice2()
	fmt.Printf("s2 -> init len: %d, cap: %d, TOTAL: %d\n", len(s2), cap(s2), TOTAL)
	for i := 0; i < 5; i++ {
		s1 = append(s2, 9)
		fmt.Printf("s2 -> update len: %d, cap: %d\n", len(s2), cap(s2))
	}
}

func main() {
	use_sub_slice()
	/** 输出:
	s1 -> init len: 5, cap: 1048574, TOTAL: 1048576
	s1 -> update len: 6, cap: 1048574
	s1 -> update len: 7, cap: 1048574
	s1 -> update len: 8, cap: 1048574
	s1 -> update len: 9, cap: 1048574
	s1 -> update len: 10, cap: 1048574
	========================================
	s2 -> init len: 5, cap: 5, TOTAL: 1048576
	s2 -> update len: 5, cap: 5
	s2 -> update len: 5, cap: 5
	s2 -> update len: 5, cap: 5
	s2 -> update len: 5, cap: 5
	*/
}
```

### 4、函数参数传递切片指针

![image-20230718224537453](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230718224537453.png)

```go
// @Author: cyl
// @File: slice指针作为函数传参.go
// @Time: 2023/07/18 22:49:09
package main

import (
	"fmt"
)

func changeSlice(arr *[]int) {
	// len和cap要变，需要传切片指针
	*arr = append(*arr, 9) // arr=[1, 2, 3, 9] cap=6
	// len、pointer和cap要变，需要传切片指针
	*arr = (*arr)[1:2] // arr=[2] cap=5
	// slice结构体里的三个Field都不变，只改变底层数组，不需要传递切片指针
	(*arr)[0] = 9 // arr=[9] len=1, cap=5
}

func testChangeSlice() {
	arr := []int{1, 2, 3}
	fmt.Printf("len: %d, cap: %d, array: %v\n", len(arr), cap(arr), arr) // len: 3, cap: 3, array: [1 2 3]
	changeSlice(&arr)
	fmt.Printf("len: %d, cap: %d, array: %v\n", len(arr), cap(arr), arr) // len: 1, cap: 5, array: [9]
}

func main3() {
	testChangeSlice()
}
```

### 5、一边遍历一边修改切片

```go
arr := []int{1, 2, 3}
// go语言中for range取得的是集合元素的拷贝
for _, le := range arr {
    ele = ele + 1
}
fmt.Println(arr) // [1, 2, 3]

for i, ele := range arr {
    arr[i] = ele + 1
}
fmt.Println(arr) // [2, 3 ,4]
```

