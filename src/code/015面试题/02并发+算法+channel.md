### 1、共享内存-基础

多线程共享内存来进行通信
通过加锁来访问共享数据，如数组、map 或 struct，go 语言也实现了这种并发模型

![内存共享基础](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230718000533251.png)

我们先看下面一段代码(memory.go)，用于验证读写锁时全局共享的变量是否可行

```go
package main

import (
	"sync"
    "time"
    "fmt"
)

var mp sync.Map

/**
读写内存的方法
读：mp.Load(key string) (interface{}, bool)
写：mp.Store(key, value string)
*/
func rwShareMemory() {
    if value, exists := mp.Load("mykey"); exists {
        fmt.Println("myvalue:", value)
    } else {
        mp.Store("mykey", "myvalue")
    }
}

func main() {
    go rwShareMemory()
    go rwShareMemory()
    go rwShareMemory()
    go rwShareMemory()
    go rwShareMemory()

    time.Sleep(time.Second)
}
```

运行代码，我们发现：明明开了四个协程，但是只输出了两项 或 三项值

```txt
~ go run memory.go
myvalue
myvalue
```

改造一下 main 函数，将协程改为同步代码，并运行该脚本

```go
func main() {
    rwShareMemory()
    rwShareMemory()
    rwShareMemory()
    rwShareMemory()
    rwShareMemory()

    time.Sleep(time.Second)
}
```

```txt
~ go run memory.go
myvalue
myvalue
myvalue
```

### 2、共享内容-CSP

csp 即 communicating sequential process，在 go 语言中就是**channel**，csp 讲究的是“以通信的方式来共享内存”

![image-20230717075417435](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230717075417435.png)

下面看一段代码(producterAndConsumer.go)

```go
package main

import (
    "fmt"
	"time"
)

type Task struct {
    A int
    B int
}

var ch = make(chan Task, 100)

func Producter() {
    for i:= i<10; i++ {
        ch <- Task{A:i+3, B: i-8}
    }
}

func Consumer() {
    for i:= i<10; i++{
        task := <- ch
        sum := task.A + task.B
        fmt.Println("sum:", sum)
    }
}

func main() {
    go Producter() // 生产者协程
    go Consumer()  // 消费者协程
    time.Sleep(time.Second)
}
```

运行代码

```txt
~ go run producterAndConsumer.go
-5
-3
-1
1
3
5
7
9
11
13
```

### 3、共享数据-协程内共享数据

写一段代码(merageFile.go)：并发的对 3 个小文件进行读写并合并到 1 个 txt 文件中

```go
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

var (
	content     = make(chan string, 1000)
	readFileCh  = make(chan struct{}, 3)
	writeFileCh = make(chan struct{}, 0)
)

func ReadFile(file string) {
	fine, err := os.Open(file)
	if err != nil {
		fmt.Println("err:", err)
	}
	defer fine.Close()
	reader := bufio.NewReader(fine)
	for {
		line, err := reader.ReadString('\n')
		if err == nil {
			content <- line
		} else {
			if err == io.EOF {
				if len(line) > 0 { // 输入文件的最后一行没有换行符
					content <- (line + "\n")
				}
				break
			} else {
				fmt.Println("err:", err)
			}
		}
	}

	// 每次读取文件后都去content的管道中获取一个元素
	<-readFileCh
	if len(readFileCh) == 0 {
		close(content)
	}
}

func WriteFile(fileWrite string) {
	fine, err := os.OpenFile(fileWrite, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0666)
	if err != nil {
		fmt.Println("err:", err)
	}
	defer fine.Close()
	writer := bufio.NewWriter(fine)
	for line := range content {
		writer.WriteString(line)
	}
	writer.Flush()

	// 写完直接给writeFileCh赋值null struct
	writeFileCh <- struct{}{}
}

func main() {
	for i := 0; i < 3; i++ {
		readFileCh <- struct{}{}
	}
	go ReadFile("data/1.txt")
	go ReadFile("data/2.txt")
	go ReadFile("data/3.txt")

	go WriteFile("data/big.txt")
	// 当代码运行时，直接运行到这里发现writeFileCh管道为空，则会一直阻塞，直至所有协程运行完毕
	// 运行过程中，当最后一个WriteFile方法运行完成后(在writeFileCh管道中塞了一个数据)，则从writeFileCh管道中读到了数据，程序结束
	<-writeFileCh
}
```

运行代码并输出

```txt
~ go run merageFile.go
4
5
6
1
2
3
```

### 4、channel 的死锁问题

1、channel 满了，就阻塞写；channe 空了，就阻塞读

2、阻塞之后会交出 cpu 执行权，去执行其他线程，希望其他线程能帮自己解决阻塞

3、如果阻塞发生在 main 协程中，**并且没有其他子协程可以执行**，那就可以确认“希望永远等不来”，自己把自己杀掉，报一个 fatal err: deadlock

4、如果阻塞发生在子协程中，就不会发生死锁，因为 main 协程至少是一个值得等待的希望，会一直等(阻塞)下去

示例 1

<img src="https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230717214808790.png" alt="image-20230717214808790" />

运行代码并执行

```txt
sub routine 1 over
```

示例 2

```go
package main

import "fmt"

func main() {
    ch := make(chan int, 3)
    go func() {
        ch <- 1
        ch <- 2
        ch < -3
        close(ch) // 删除该行报错: deadlock
    }()
    for ele := range ch {
        fmt.Println(ele)
    }
    fmt.Println("bye~")
}
```

解释：如果删掉`close(ch)`代码，那么 for 循环会一直去 ch 管道中遍历获取数据，但是由于管道并没有关闭，所以在取完 123 三个值之后，报错：fatal error: deadlock

### 5、进程与线程、协程

进程与线程

![image-20230717220012501](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230717220012501.png)

并发模型

![image-20230717220640866](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230717220640866.png)

协程和线程的概念

![image-20230717220737812](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230717220737812.png)

协程和线程切换的成本区别

![image-20230717222057307](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230717222057307.png)

协程和线程的创建和销毁

![image-20230717222656325](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230717222656325.png)

### 6、go 语言中的 MPG 并发模型

![image-20230717222901660](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230717222901660.png)

启动一个线程（用不同编程语言实现）

java

```java
java
class MyThead extends Thread {
	public void run() {}
}
MyThread t = new MyThread()
t.start()
```

python

```python
import threading

class MyThread(threading.Thread):
    def run(self): pass

t = MyThread()
t.start()
```

c++

```c++
#include <thread>
std::thread t(add, 2, 4)
```

golang

```go
go add(2, 4)
```

来，跑个测试，基础任务：100\*100 的双精度矩阵，用[0,1]上的随机数初始化，自己与自己相乘，需要 100 万次乘法

机器配置：centos 云主机，8c16g 内存

对比：java vs golang，并发执行矩阵乘法

```java
public void run() {
    double[][] rect = new double[100][100];
    for(int i=0;i<100;i++) {
        for(int j=0, j<100; j++) {
            double prod = 0;
            for(int k=0, k<100; k++) {
                prod += A[i][k] * B[j][k];
            }
            rect[i][j] = prod;
        }
    }
}
```

```go
func MatrixMultiply(A, B [100][100]float64) {
    rect := [100][100]float64{}
    for i:=0; i<100; i++ {
        for j:=0; j < 100; j++ {
            var prod float64 = 0.0
            for k:=0; k<100; k++ {
                prod += A[i][k] * B[j][k]
            }
            rect[i][j] = prod
        }
    }
}
```

![image-20230717225639890](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230717225639890.png)

### 7、资源竞争

多协程并发修改同一块内存，产生资源竞争

go run 或 go build 时添加-race 参数检查资源竞争情况

例如：n++不是原子操作，并发执行时会存在脏写；n++分为三步：取出 n，再加 1，再赋值给 n；测试时需要开 1000 个并发才能观察到脏写

![image-20230717230410594](https://gitee.com/mathmost/go_study/raw/master/uPic/image-20230717230410594.png)

如果想解决上述问题，把 n++封装成`原子操作`，解除资源竞争，避免脏写

```go
func atomic.AddInt32(add *int32, delta int32) (new int32)
func atomic.LoadInt32(add *int32) (val int32)
```

### 8、读写锁

var lock sync.RWMutex 声明读写锁，无需初始化

lock.Lock(), lock.Unlock() 加写锁和释放写锁

lock.RLock(), lock.RUnlock() 加读锁和释放读锁

任意时刻只可以加一把写锁，且不能加读锁

没加写锁时，可以同时加多把读锁，读锁加上后就不能再加写锁了

### 9、容器的并发安全性

数组、slice、struct 允许并发修改(可能会脏写)，并发修改 map 有时会发生 panic，如果需要并发修改 map，需要使用 sync.Map

### 10、sync.Once

确保在高并发的场景下只执行一次，比如：加载配置文件、关闭管道等

```go
var resource map[string]string
var loadResourceOnce sync.Once

func LoadResource() {
    loadResourceOnce.Do(func() {
        resource["1"] = "A"
    })
}
```

sync.Once 比较常见用于实现单例模式

```go
type Singleton struct{}

var singleton *Singleton
var singletonOnce *sync.Once

func GetSingletonInstance() *Singleton {
    singletonOnce.Do(func() {
        singleton = &Singleton{}
    })
    return singleton
}
```
