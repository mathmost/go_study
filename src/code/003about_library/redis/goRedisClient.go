package main

import (
	"fmt"

	"github.com/go-redis/redis"
)

var rdb *redis.Client

func init() {
	rdb = redis.NewClient(&redis.Options{
		Network:  "tcp",               // 网络类型: [tcp, unix], 默认是tcp
		Addr:     "127.0.0.1:6379123", // 防止冲突, 这里port故意写错
		Password: "",
		DB:       1,
	})
	// 通过client.Ping()检查是否链接成功
	ping, err := rdb.Ping().Result()
	fmt.Println("new client ping, err:", ping, err)
}

func NewRedisClientUsedemo() {
	// 0代表永不过期
	err := rdb.Set("gorediskey", "goredisvalue", 0).Err()
	if err != nil {
		panic(err)
	}
	value, err := rdb.Get("gorediskey").Result()
	if err != nil {
		panic(err)
	}
	fmt.Println("gorediskey", value)

	// hset hget
	value2, err := rdb.HSet("goredishsetkey", "name", "上海").Result()
	if err != nil {
		panic(err)
	}
	fmt.Println("goredishsetkey:", value2)
}
