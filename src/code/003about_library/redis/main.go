package main

func main() {
	// 1. 连接数据库
	conn, err := goRedisConnDial("127.0.0.1:6379", "", 0)
	if err != nil {
		return
	}
	defer conn.Close()

	// 2. redis操作string
	goRedisString(conn)

	// 3. redis操作hash
	goRedisHash(conn)

	// 4. redis操作list
	goRedisList(conn)

	// 5. redis链接池应用
	goRedisClientPool()

	// 最后: 清空当前数据库所有的key
	goRedisFlushDb(conn)
}
