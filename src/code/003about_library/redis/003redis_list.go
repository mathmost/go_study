package main

import (
	"fmt"

	"github.com/gomodule/redigo/redis"
)

/**
go redis 操作list类型
	lpush: 从列表左边插入数据
	lpushx: 跟LPush的区别是，仅当列表存在的时候才插入数据,用法完全一样
	rpop: 从列表的右边删除第一个数据，并返回删除的数据
	rpush: 从列表右边插入数据
	rpushx: 跟RPush的区别是，仅当列表存在的时候才插入数据, 他们用法一样
	lpop: 从列表左边删除第一个数据，并返回删除的数据
	llen: 返回列表的大小
	lrange: 返回列表的一个范围内的数据，也可以返回全部数据
	lrem: 删除列表中的数据
	lindex: 根据索引坐标，查询列表中的数据
	linsert: 在指定位置插入数据
*/

func goRedisList(conn redis.Conn) {
	// lpush(key, value interface{})
	_, err := conn.Do("lpush", "herolist", "10", "20", "30", "40", "50", "60")
	if err != nil {
		fmt.Println("err:", err)
	}

	// llen(key)
	lpushLLen, _ := redis.Int(conn.Do("llen", "herolist"))
	fmt.Printf("lpushLLen:%d, %T\n", lpushLLen, lpushLLen)

	// lrange(key, start, end), 比如: lrange key 0 -1
	lpushLRange, err := redis.Strings(conn.Do("lrange", 0, -1))
	if err != nil {
		fmt.Println("lpushLRange error:", err) // ERR wrong number of arguments for 'lrange' command
	}
	fmt.Println("lpushLRange:", lpushLRange)

	// lpop(key)
	lpushRPop, _ := redis.String(conn.Do("rpop", "herolist"))
	fmt.Println("lpushRPop:", lpushRPop)

	// lrem(key)
	// conn.Do("lrem", "herolist", 1, 100)
}
