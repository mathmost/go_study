package main

import (
	"fmt"

	"github.com/gomodule/redigo/redis"
)

/** go redis 操作hash类型: 适合批量数据写入和读取 */

func goRedisHash(conn redis.Conn) {
	// 1. 单个set get: 假设有一个人user01, 分别有姓名和年龄
	_, hashNameErr := conn.Do("HSet", "user01", "name", "John")
	if hashNameErr != nil {
		fmt.Println("hset error:", hashNameErr)
		return
	}
	_, hashAgeErr := conn.Do("HSet", "user01", "age", 18)
	if hashAgeErr != nil {
		fmt.Println("hset error:", hashAgeErr)
		return
	}
	r4, e4 := redis.String(conn.Do("HGet", "user01", "name"))
	r5, e5 := redis.Int(conn.Do("HGet", "user01", "age"))
	fmt.Printf("r4:%v, %T, e4:%v\n", r4, r4, e4)
	fmt.Printf("r5:%v, %T, e5:%v\n", r5, r5, e5)

	// 2. 批量set get: 一对一对的形式
	_, mHashErr := conn.Do("MSet", "name", "姓名Mary", "address", "地址shanghai", "height", 178.6, "sex", "性别男")
	if mHashErr != nil {
		fmt.Println("mset error:", hashAgeErr)
		return
	}
	r6, _ := redis.Strings(conn.Do("MGet", "name", "address", "height", "sex"))
	for _, v := range r6 {
		fmt.Println("mget value:", v)
	}
}
