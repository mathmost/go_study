package main

import (
	"fmt"

	"github.com/gomodule/redigo/redis"
)

/**
redis链接池
	1. 先创建好链接池，客户端发出指令，从链接池中取出一个链接使用
	2. 当再发出一个链接池，再取出一个链接使用
	3. 这样可以节省临时获取redis链接的时间，从而提高效率
	4. 代码示例:
		var pool *redis.Pool
		pool = &redis.Pool{
			MaxIdle: 8, // 最大空闲链接数
			MaxActive: 0, // 表示和数据库最大链接数 0表示没有限制
			IdleTimeout: 100, // 最大空闲时间，单位：s，ps: 假如一个链接在100s内中没有被链接，则会重新放回链接池
			Dial: func() (redis.Conn, error) { // 初始化链接池
				return redis.Dial("tcp", "localhost:6379")
			}
		}
		c := pool.Get() // 从链接池中取出一个链接
		pool.Close() // 关闭链接池(一旦关闭链接池，就不能从链接池再取出链接)
*/

func goRedisClientPool() {
	pool := &redis.Pool{
		MaxIdle:     8,   // 最大空闲链接数
		MaxActive:   0,   // 表示和数据库最大链接数 0表示没有限制
		IdleTimeout: 100, // 最大空闲时间，单位：s，ps: 假如一个链接在100s内中没有被链接，则会重新放回链接池
		Dial: func() (redis.Conn, error) { // 初始化链接池
			return redis.Dial("tcp", "localhost:6379")
		},
	}
	conn := pool.Get()
	defer conn.Close()
	// set
	_, seterr := conn.Do("set", "pool_hello", "world")
	if seterr != nil {
		fmt.Println("set key err:", seterr)
		return
	}
	// get
	r, get_err := conn.Do("get", "pool_hello")
	if get_err != nil {
		fmt.Println("get key err:", get_err)
		return
	}
	fmt.Println("redis pool result:", r) // world
}
