package main

import (
	"fmt"

	"github.com/gomodule/redigo/redis"
)

/** go语言redis操作方式一 */
func goRedisConnDial(seiverHost, pwd string, dbIndex int) (c redis.Conn, err error) {
	/**
	1. 连接数据库: redis.Dial(协议, "IP:端口"") 或 redis.Dial(协议, ":端口"")
				  简写形式: conn, err := redis.Dial("tcp", ":6379")
		a、func Dial(network, address string, options ...DialOption) (Conn, error)
			network网络协议， 一般指定tcp即可
			address 为连接字符串 比如’127.0.0.1:6379’
			默认情况下， 没有通过DailOption指定db， 则db默认为0
			DialOption 为一些连接参数，定义如下：
				type DialOption struct {
					f func(*dialOptions)
				}
				type dialOptions struct {
					readTimeout  time.Duration
					writeTimeout time.Duration
					dialer       *net.Dialer
					dial         func(network, addr string) (net.Conn, error)
					db           int
					password     string
					clientName   string
					useTLS       bool
					skipVerify   bool
					tlsConfig    *tls.Config
				}
			DialOption 其实封装了dialOptions， 对应参数的修改也提供量一些对应的方法，根据参数名，可以很方便地找到；
			比如，db参数设置func DialDatabase(db int) DialOption 使用DialDatabase即可设置
		b、DialOption 方法
			DialClientName：func DialClientName（name string）DialOption指定Redis服务器连接使用的客户端名称
			DialConnectTimeout：func DialConnectTimeout(d time.Duration) DialOption 指定在未指定DialNetDial选项时连接到Redis服务器的超时
			DialDatabase：func DialDatabase（db int）DialOption 指定连接时要选择的数据库
			DialKeepAlive：func DialKeepAlive(d time.Duration) DialOption指定在未指定DialNetDial选项时与Redis服务器的TCP连接的保持活动期。如果为零，则不启用keep-alives。如果未指定DialKeepAlive选项，则使用默认值5分钟来确保检测到半封闭的TCP会话
			DialNetDial：func DialNetDial(dial func(network, addr string) (net.Conn, error)) DialOption；指定用于创建TCP连接的自定义拨号功能，否则使用通过其他选项自定义的net.Dialer。DialNetDial会覆盖DialConnectTimeout和DialKeepAlive
			DialPassword：func DialPassword(password string) DialOption 连接到Redis服务器的密码
			DialReadTimeout：func DialReadTimeout(d time.Duration) DialOption 指定读取单个命令回复超时时间
			DialTLSConfig：func DialTLSConfig(c *tls.Config) DialOption 指定在TLS连接时使用的配置
			DialTLSSkipVerify：func DialTLSSkipVerify（skip bool）DialOption 通过TLS连接时，DialTLSSkipVerify禁用服务器名称验证
			DialUseTLS：func DialUseTLS（useTLS bool）DialOption 是否应使用TLS。DialURL忽略此选项
			DialWriteTimeout：func DialWriteTimeout(d time.Duration) DialOption 写入单个命令的超时时间
		c、参考链接
			https://blog.csdn.net/comprel/article/details/95920188
	*/
	setDb := redis.DialDatabase(dbIndex) // 设置DB
	setPasswd := redis.DialPassword(pwd) // 设置redis链接密码
	redis.DialReadTimeout(500)
	conn, err := redis.Dial("tcp", seiverHost, setDb, setPasswd)
	if err != nil {
		fmt.Println("redis Dial failed, err:", err)
	}
	return conn, err
}

func goRedisFlushDb(conn redis.Conn) {
	// 清空当前数据库成功
	_, err := conn.Do("flushdb")
	if err != nil {
		fmt.Println("flushdb error:", err)
	}
}
