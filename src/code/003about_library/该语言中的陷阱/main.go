//@Author: cyl
//@File: main.go
//@Time: 2023/06/25 21:19:08
package main

import "fmt"

/**
go语言陷阱的分类
1、数据接口细节：切片的拷贝和扩容、map的存储和并发访问
2、闭包与循环变量的特性，协程泄漏通道死锁
3、程序与代码组织循环依赖，依赖注入
*/

func ChangeArray(new []int) {
	new[0] = 100
}

func main() {
	// 1、我们来看一个最简单的陷阱
	var old = []int{1, 2, 3}
	ChangeArray(old)
	fmt.Println("old:", old) // [100 2 3]

	// 我们来查看slice底层的数据接口
	type SliceHeader struct {
		Data uintptr // slice元素对应数组的地址
		Len  int     // 代表slice的长度
		Cap  int     // 代表slice的容量
	}

	// 2、新特性陷阱-闭包，以for和range举例
	for _, v := range old {
		go func() {
			fmt.Println("v:", v)
		}()
	}
}
