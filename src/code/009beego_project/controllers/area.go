package controllers

import (
	"fmt"
	"hello/src/code/009beego_project/models"
	"hello/src/code/009beego_project/utils"

	"github.com/astaxie/beego/orm"
)

type AreaController struct {
	// beego.Controller
	utils.ResponseController
}

func (area *AreaController) Get() {
	var areas []models.Area
	o := orm.NewOrm()
	num, err := o.QueryTable("area").All(&areas)
	if num <= 0 {
		fmt.Println("查询area数据为空", err)
		area.ErrorJson(utils.RECODE_NODATA, nil)
		return
	}
	if err != nil {
		fmt.Println("查询area数据库失败", err)
		area.ErrorJson(utils.RECODE_DBERR, nil)
		return
	}
	area.SuccessJson("OK", areas)
}
