package controllers

import (
	"encoding/json"
	"fmt"
	"hello/src/code/009beego_project/models"
	"hello/src/code/009beego_project/utils"

	"github.com/astaxie/beego/orm"
)

type UserController struct {
	// beego.Controller
	utils.ResponseController
}

// 用户注册
func (user *UserController) Post() {
	fmt.Println("................ user register post ................")
	var params = make(map[string]interface{})
	// 获取前端传递的json数据
	json.Unmarshal(user.Ctx.Input.RequestBody, &params)
	mobile := params["mobile"]
	passwd := params["password"]
	sms_code := params["sms_code"]
	// 校验参数
	if mobile == "" && passwd == "" && sms_code == "" {
		user.ErrorJson(utils.RECODE_PARAMERR, nil)
	}
	// 插入数据库
	o := orm.NewOrm()
	u := models.User{}
	u.Name = mobile.(string)
	u.Mobile = mobile.(string)
	u.Password_hash = passwd.(string)
	id, err := o.Insert(&u)
	if err != nil {
		user.ErrorJson(utils.RECODE_DATAERR, nil)
		return
	}
	fmt.Println("insert user id:", id)
	user.SuccessJson("注册成功", &u)
}
