package models

import (
	"fmt"

	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	_ "github.com/go-sql-driver/mysql"
)

func MysqlDBInit() {
	driverName := beego.AppConfig.String("mysqlDriverName")
	// 注册数据库驱动
	orm.RegisterDriver(driverName, orm.DRMySQL)
	// 数据库连接
	user := beego.AppConfig.String("mysqlUsername")
	passwd := beego.AppConfig.String("mysqlPassword")
	host := beego.AppConfig.String("mysqlHost")
	port := beego.AppConfig.String("mysqlPort")
	db := beego.AppConfig.String("mysqlDb")

	// "root:yourSqlPasswd@tcp(127.0.0.1:3306)/yourSqlDb?charset=utf8"
	// 绑定orm此时用的是哪个数据库的驱动
	// 第四个参数表示 数据连接最大的空闲个数(可选)
	// 第五个参数表示 数据库最大的链接个数(可选)
	dbConn := user + ":" + passwd + "@tcp(" + host + ":" + port + ")/" + db + "?charset=utf8"
	err := orm.RegisterDataBase("default", driverName, dbConn, 30)
	if err != nil {
		fmt.Println("mysql connect err:", err)
		return
	}
	fmt.Println("mysql connect success")
	// 设置mysql超时时间
	orm.SetMaxIdleConns("default", 10)
	orm.SetMaxOpenConns("default", 100)

	// 注册model: 注册orm都有哪些模块， 目前orm需用同步哪些表
	orm.RegisterModel(new(User), new(Area), new(Facility), new(House), new(HouseImage), new(OrderHouse))

	// 创建表
	// 第二个参数表示是否强制替换
	// 第三个表示 如果没有是否创建
	orm.RunSyncdb("default", false, true)
}

func Create(param interface{}) (int64, error) {
	return orm.NewOrm().Insert(param)
}

func Update(param interface{}, fields ...string) (int64, error) {
	return orm.NewOrm().Update(param, fields...)
}

func Delete(param interface{}, cols ...string) (int64, error) {
	return orm.NewOrm().Delete(param, cols...)
}

func Read(md interface{}, cols ...string) error {
	return orm.NewOrm().Read(md, cols...)
}
