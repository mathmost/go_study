package main

import (
	"hello/src/code/009beego_project/models"
	_ "hello/src/code/009beego_project/routers"

	"github.com/astaxie/beego"
)

func main() {
	// 初始化mysql
	models.MysqlDBInit()

	beego.Run()
}
