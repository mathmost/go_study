package routers

import (
	"hello/src/code/009beego_project/controllers"

	"github.com/astaxie/beego"
)

func init() {
	beego.Router("/", &controllers.MainController{})

	// 地址信息
	beego.Router("/api/v1.0/areas", &controllers.AreaController{})

	// beego.Router("/api/v1.0/session", &controllers.SessionController{}, "get:Get")

	// 用户注册
	beego.Router("/api/v1.0/users", &controllers.UserController{})

	// 用户登陆
	// beego.Router("/api/v1.0/sessions", &controllers.UserController{}, "post:Login")

	// 更新用户名
	// beego.Router("/api/v1.0/user/name", &controllers.UserController{}, "put:UpdateUsername")

	// 上传用户文件头像
	// beego.Router("/api/v1.0/user/avatar", &controllers.UserController{}, "post:GetAvatar")
}
