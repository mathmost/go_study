package utils

import (
	"github.com/astaxie/beego"
)

type ResponseController struct {
	beego.Controller
}

type ReturnMsg struct {
	Code string      `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

func (r *ResponseController) SuccessJson(msg string, data interface{}) {
	if msg == "" {
		msg = "success"
	}
	resp := ReturnMsg{
		Code: RECODE_OK, // 200
		Msg:  msg,
		Data: data,
	}
	r.Data["json"] = resp
	r.ServeJSON()
	r.StopRun()
}

func (r *ResponseController) ErrorJson(code string, data interface{}) {
	reNewMsg := RecodeText(code)
	resp := ReturnMsg{
		code, reNewMsg, data,
	}
	r.Data["json"] = resp
	r.ServeJSON()
	r.StopRun()
}
