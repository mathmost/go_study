package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func run() {
	// 1. 初始化路由：创建一个默认的路由引擎
	r := gin.Default()
	// 2. 使用LoadHTMLGlob()或者LoadHTMLFiles()方法进行HTML模板渲染
	// templates文件夹的位置一定要正确匹配，否则引擎无法找到需要渲染的html
	r.LoadHTMLGlob("../../templates/**/*")
	// r.LoadHTMLFiles("templates/posts/index.html", "templates/users/index.html")

	// 3. GET/POST/PUT/DELETE：请求方式 + 返回类型
	// 3.1 返回字符串
	r.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "返回一个字符串, hello world!")
	})

	// 3.2 json渲染: 直接返回json格式
	r.GET("/hello", func(c *gin.Context) {
		// Context: 表示gin框架中的上下文
		c.JSON(200, gin.H{
			"message": "Hello World!",
		})
	})

	// 3.3 json渲染: 结构体转json
	r.GET("/book", func(c *gin.Context) {
		var payload struct {
			Name    string `json:"user"`
			Message string
			Age     int
		}
		payload.Name = "小王子"
		payload.Message = "Hello world!"
		payload.Age = 18
		c.JSON(http.StatusOK, payload)
	})

	// 3.4 返回xml
	r.GET("/xml", func(ctx *gin.Context) {
		ctx.XML(http.StatusOK, gin.H{
			"user": "hello", "msg": "world", "status": http.StatusOK,
		})
	})

	// 3.5 返回yaml
	r.GET("/yaml", func(ctx *gin.Context) {
		ctx.YAML(http.StatusOK, gin.H{
			"user": "hello", "msg": "world", "status": http.StatusOK,
		})
	})

	// 3.6 返回html(模板渲染)
	r.GET("/posts/index", func(c *gin.Context) {
		c.HTML(http.StatusOK, "posts/index.html", gin.H{
			"title": "posts index",
		})
	})

	r.GET("/users/index", func(c *gin.Context) {
		c.HTML(http.StatusOK, "users/index.html", gin.H{
			"title": "users index",
		})
	})

	// 3.7 其它请求类型
	r.POST("/create_book", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "Post",
		})
	})

	r.PUT("/book", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "Put",
		})
	})

	r.DELETE("/book", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "Delete",
		})
	})

	// 4. 启动HTTP服务，默认在0.0.0.0:8080启动服务
	r.Run("8080")
}
