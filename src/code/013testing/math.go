//@Author: cyl
//@File: math.go
//@Time: 2023/07/02 18:15:23
package main

func Add(n1, n2 int) int {
	return n1 + n2
}

func Sub(n1, n2 int) int {
	return n1 - n2
}

func Chen(n1, n2 int) int {
	return n1 * n2
}

func Chu(n1, n2 int) int {
	return n1 / n2
}
