//@Author: cyl
//@File: math_test.go
//@Time: 2023/07/02 18:16:41
package main

import (
	"fmt"
	"testing"
)

func TestMathFunc(t *testing.T) {
	fmt.Println("testing add:", Add(10, 20))
	addResult := Add(10, 20)
	addExcept := 30
	if addResult != addExcept {
		t.Errorf("add(10, 20) result=%d, except=%d", addResult, addExcept)
	}
	fmt.Println("testing sub:", Sub(10, 20))
	fmt.Println("testing chen:", Chen(10, 20))
	fmt.Println("testing chu:", Chu(10, 20))
}
