package blog

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func goodsHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "goods!",
	})
}

func checkoutHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"message": "checkout!",
	})
}
