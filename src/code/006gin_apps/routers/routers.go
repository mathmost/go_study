package routers

import (
	_ "hello/src/code/006gin_apps/docs" // 导入生成的docs

	"github.com/gin-gonic/gin"
	// "github.com/swagger/gin-swagger/swaggerFiles"
)

type Option func(*gin.Engine)

var options = []Option{}

// 注册app的路由配置
func Include(opts ...Option) {
	options = append(options, opts...)
}

// 初始化
func Init() *gin.Engine {
	r := gin.Default()
	for _, opt := range options {
		opt(r)
	}
	// r.GET("/swagger/*any", gs.WrapHandler(swaggerFiles.Handler)) // 注册swagger api相关路由
	return r
}
