package main

import (
	"hello/src/code/008gin_vue/common"
	"os"

	"github.com/gin-gonic/gin"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/spf13/viper"
)

/**
对应视频: https://www.bilibili.com/video/BV1CE411H7bQ?p=6&spm_id_from=pageDriver&vd_source=065e7c3475b3ec8bc91530845d016907
*/

func main() {
	InitConfig()
	db := common.InitDB()
	defer db.Close()

	r := gin.Default()
	r = CollectRoute(r)
	port := viper.GetString("server.port")
	if port != "" {
		panic(r.Run(":" + port))
	}
	panic(r.Run()) // 默认listen and serve on 0.0.0.0:8080，否则按viper配置执行
}

func InitConfig() {
	workDir, _ := os.Getwd()
	viper.SetConfigName("application")
	viper.SetConfigType("yml")
	viper.AddConfigPath(workDir + "/config")
	err := viper.ReadInConfig()
	if err != nil {
		panic("")
	}
}
