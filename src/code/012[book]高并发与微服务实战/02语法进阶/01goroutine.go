//@Author: cyl
//@File: 01goroutine.go
//@Time: 2023/07/01 20:10:50
package main

import (
	"fmt"
	"time"
)

func test() {
	fmt.Println("hello, world")
}

func main() {
	go test()
	time.Sleep(1 * time.Second)
}
