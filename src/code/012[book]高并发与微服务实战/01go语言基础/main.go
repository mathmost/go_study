package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"strconv"
)

// 请求体结构体
type requestBody struct {
	Key    string `json:"key"`
	Info   string `json:"info"`
	UserId string `json:"userid"`
}

// 结果体结构体
type responseBody struct {
	Code int      `json:"code"`
	Text string   `json:"text"`
	List []string `json:"list"`
	Url  string   `json:"url"`
}

// 请求机器人
func process(inputChan <-chan string, userid string) {
	for {
		// 从通道中接收输入
		input := <-inputChan
		if input == "EOF" {
			break
		}
		// 构建请求体
		reqData := &requestBody{
			Key:    "792bcf45156d488c92e9d11da494b085",
			Info:   input,
			UserId: userid,
		}
		byteData, _ := json.Marshal(&reqData)
		// 请求接口
		req, err := http.NewRequest(
			http.MethodPost,
			"http://www.tuling123.com/openapi/api",
			bytes.NewReader(byteData),
		)
		if err != nil {
			return
		}
		req.Header.Set("Content-Type", "application/json;charset=UTF-8")
		client := http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			return
		} else {
			body, _ := ioutil.ReadAll(resp.Body)
			var respData responseBody
			json.Unmarshal(body, &respData)
			fmt.Println("AI:", respData.Text)
		}
		defer resp.Body.Close()
	}
}

func main() {
	var input string
	// 创建通道
	channel := make(chan string)
	// main结束时关闭通道
	defer close(channel)
	// 启动协程运行机器人互动线程
	randUserId := strconv.Itoa(rand.Int())
	go process(channel, randUserId)
	for {
		fmt.Scanf("%s", &input)
		channel <- input
		// 结束程序
		if input == "EOF" {
			fmt.Println("Bye")
			break
		}
	}
}
