// @Author: cyl
// @File: main.go
// @Time: 2022/06/09 22:07:52
package main

import (
	"fmt"
)

/*
*
 1. make和new都是用来分配内存的
 2. new很少用，一般用来给基本数据类型申请内存，string/int/bool/struct，
    返回的是对应的这三个类型指针(如*string, *int)
 3. make用于给slice map chan申请内存的，返回的是对应的这三个类型本身
*/
func main() {
	var a = make([]int, 5, 10)
	i := 0
	for i < 10 {
		a = append(a, i)
		i++
	}
	fmt.Println("a:", a) // [0 0 0 0 0 0 1 2 3 4 5 6 7 8 9]

	var b = make(map[string]int)
	b["牛魔王"] = 18
	b["白晶晶"] = 25
	fmt.Println("b:", b) // map[牛魔王:18 白晶晶:25]
}
