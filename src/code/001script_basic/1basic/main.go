package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello Go!")
	fmt.Println("Hello World!")
	fmt.Println("Hello Python!")

	// 数字: 表示很大的数
	var distance = 24e18
	fmt.Printf("distance: %T, %v\n", distance, distance) // distance: float64, 2.4e+19
	// 如果没有为指数形式的数值指定类型的话，那么go会将其作为float64类型

	fmt.Println(" ----- 变量 -----")
	// 类型推导
	var name string
	var age = 18

	// 批量声明
	var (
		a string
		b int
		c bool
		d float32
	)

	// 一次初始化多个变量
	var e, f = "QM", 24
	n := "小王子"
	p := 6

	// 匿名变量
	x, _ := foo()
	_, y := foo()
	fmt.Println("x = ", x) // x = 10
	fmt.Println("y = ", y) // y = Q1mi

	fmt.Println("定义值为: ", name, age, e, f, n, p) // None 18 QM 24 小王子 6
	fmt.Println(a, b, c, d)                      // None 0 false 0

	fmt.Println(" ----- 常量 -----")
	const sn = 100
	const (
		n1 = 100
		n2
		n3
	)
	fmt.Println(sn, n1, n2, n3) // 100 100 100 100

	// iota可以裂解为const的索引
	const (
		p1 = iota
		p2
		p3
		_
		p5
		p6     = 100
		p7, p8 = iota + 1, iota + 2
	)
	fmt.Println("iota", p1, p2, p3, p5, p6, p7, p8) // 0 1 2 4 100 7 8

	// fmt.Println("----- for -----")
	// s := "hello python"
	// for _, r := range s {
	// 	fmt.Printf("%c\n" , r)
	// }

	// switch case
	// var inputValue byte
	// fmt.Println("请输入一个字符: a b c d e f g")
	// fmt.Scanf("%c", &inputValue)
	// switch inputValue {
	// case 'a':
	// 	fmt.Println("周一")
	// case 'b':
	// 	fmt.Println("周一")
	// case 'c':
	// 	fmt.Println("周一")
	// default:
	// 	fmt.Println("输入有误")
	// }

	// 逻辑运算符
	// 与&&：只要有一个为false，结果为false
	// 或||：只要有一个为true，结果为true
	// 非!：取反
	// fmt.Println(true && true)   // true
	// fmt.Println(true && false)  // false
	// fmt.Println(false && true)  // false
	// fmt.Println(false && false) // false

	// fmt.Println(true || true)   // true
	// fmt.Println(true || false)  // true
	// fmt.Println(false || true)  // true
	// fmt.Println(false || false) // false

	// fmt.Println(!true)  // false
	// fmt.Println(!false) // true

	// 求数组[1, 3, 5, 7, 8]所有元素的和
	sum := 0
	var array_list = [5]int{1, 3, 5, 7, 8}
	for _, value := range array_list {
		sum += value
	}
	fmt.Println("array sum is: ", sum)
	// 找出数组中和为指定值的两个元素的下标，比如从数组[1, 3, 5, 7, 8]中找出和为8的两个元素的下标分别为(0,3)和(1,2)
	for i := 0; i < len(array_list); i++ {
		for j := i + 1; j < len(array_list); j++ {
			if array_list[i]+array_list[j] == 8 {
				fmt.Println(i, j)
			}
		}
	}

	// 切片
	// 切换初始化才能使用
	var qp = []int{1, 2, 3, 4, 5, 6}
	qaVal := qp[1:3]
	fmt.Println(qaVal)      // [2 3]
	fmt.Println(len(qaVal)) // 2
	fmt.Println(cap(qaVal)) // 5

	// 切片赋值
	s1 := make([]int, 3)
	s2 := s1 //将s1直接赋值给s2，s1和s2共用一个底层数组
	s2[0] = 100
	fmt.Println(s1) //[100 0 0]
	fmt.Println(s2) //[100 0 0]

	// 切片append
	var s3 []int
	s3 = append(s3, 1, 2, 3)
	fmt.Println(s3) // [1 2 3]

	var b1 = []int{4, 5, 6}
	s3 = append(s3, b1...)
	fmt.Println(s3) // [1 2 3 4 5 6]
}

func foo() (int, string) {
	return 10, "Q1mi"
}
