// @Author: cyl
// @File: main.go
// @Time: 2022/06/07 22:54:49
package main

import (
	"container/list"
	"fmt"
)

/*
*
1. go语言的列表通过双向链表的方式实现，能够高效的进行元素的插入和删除操作
2. 双向链表中每个元素都持有前后元素的索引，在链表进行向前或者向后遍历时会非常方便
3. 但是注意，进行元素的插入和删除操作时需要同时修改前后元素的持有引用
*/
func main() {
	tmpList := list.New()
	for i := 1; i <= 10; i++ {
		tmpList.PushBack(i)
	}

	first := tmpList.PushFront(0)
	tmpList.Remove(first)

	for l := tmpList.Front(); l != nil; l = l.Next() {
		fmt.Print(l.Value, " ") // 1 2 3 4 5 6 7 8 9 10
	}
}
