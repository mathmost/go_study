package main

import (
	"fmt"

	"golang.org/x/crypto/bcrypt"
)

// 处理加密和验证

func main() {
	pwd := "123456"
	// 加密
	pwd_hash, err := bcrypt.GenerateFromPassword([]byte(pwd), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println("加密失败", err)
	}

	// 验证: 旧密码 vs 需要验证的密码
	if bcrypt.CompareHashAndPassword([]byte(pwd), []byte(pwd_hash)); err != nil {
		fmt.Println("密码不一致", err)
	}
}
