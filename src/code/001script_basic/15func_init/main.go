// @Author: cyl
// @File: main.go
// @Time: 2023/06/20 23:18:25
package main

import (
	"fmt"
	_ "hello/src/code/001script_basic/15func_init/a_module"
	b "hello/src/code/001script_basic/15func_init/b_module"
	c "hello/src/code/001script_basic/15func_init/c_module"
)

/** init函数初始化
总结:
	1、一个模块下可以有多个init初始化函数
	2、在主程序中仍然会更新导入包的顺序，挨个执行模块下的init函数
	3、在主程序中导入多init初始化函数的模块时，会根据已经排好序的文件挨个执行init函数
	也就是说，主程序中会优先找模块，然后找模块下的init函数，依次执行!!!
*/

func main() {
	fmt.Println("b sum:", b.BSum(10, 20))
	fmt.Println("c sub:", c.CSub(10, 20))
	/**
	输出:
		我是a-a1函数的调用
		我是a-a3函数的调用
		我是a-a2函数的调用
		我是b-b1函数的调用
		我是b-b1函数的调用
		b sum: 30
	*/
}
