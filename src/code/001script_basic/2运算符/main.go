package main

import "fmt"

// 运算符
func main() {
	// 练习
	// 有一堆数字，如果除了一个数字以外，其他数字都出现了两次，那么如何找到出现一次的数字？
	var (
		a int = 10
		b int = 10
		c int = 20
		d int = 30
		e int = 20
		f int = 50
		g int = 30
	)
	// 使用哈希表
	var num = make(map[int]bool)
	var numList = [...]int{a, b, c, d, e, f, g}
	for _, v := range numList {
		if !num[v] {
			num[v] = true
		} else {
			fmt.Println(v)
		}
	}
}
