package main

import "fmt"

/** 嵌套 以及 结构体绑定方法 */

type AoDiDriver struct {
	Color string
	Brand string
}

type BaoMaDriver struct {
	Color string
	Brand string
}

type CatTypes struct {
	Name        string
	Age         int
	AoDiDriver  AoDiDriver
	BaoMaDriver BaoMaDriver
}

func (aodi AoDiDriver) colorFunc() {
	fmt.Println("奥迪汽车的颜色:", aodi.Color)
}

func (aodi BaoMaDriver) brandFunc() {
	fmt.Println("宝马汽车的品牌1:", aodi.Brand)
}

func (aodi BaoMaDriver) brandFuncParam(a, b int) {
	fmt.Println("宝马汽车的品牌2:", aodi.Brand)
	fmt.Println("a + b:", a+b)
}

func (ct CatTypes) sayHello() {
	fmt.Println("name and age:", ct.Name, ct.Age)
	ct.AoDiDriver.colorFunc()
	ct.BaoMaDriver.brandFunc()
	ct.BaoMaDriver.brandFuncParam(10, 20)
}
