//@Author: cyl
//@File: zerovalue.go
//@Time: 2023/06/04 21:39:38
package main

import (
	"fmt"
)

// 0值：初始化
type House struct {
	Name   string
	Height int
	Level  int
}

// 初始化实例对象0值的几种方式
func ZeroValueType() {
	var c1 House = House{}
	var c2 House
	var c3 *House = new(House)

	fmt.Println(c1.Height) // 0
	fmt.Println(c2.Height) // 0
	fmt.Println(c3.Height) // 0
}
