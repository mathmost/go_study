//@Author: cyl
//@File: niming.go
//@Time: 2022/06/10 21:01:35
package main

import (
	"fmt"
)

// 结构体-匿名字段
// ·相同类型只能写一个·，适用于字段比较少且简单的场景，不常用
type PersonNiMing struct {
	string
	int
}

func NiMingFunc() {
	var p = PersonNiMing{
		"牛魔王",
		200,
	}
	fmt.Println("p:", p)
	fmt.Println("p.string:", p.string)
	fmt.Println("p.int:", p.int)
}
