package main

import "fmt"

type Student struct {
	Name  string
	Age   int
	Score float64
}

func (stu *Student) getInfo() {
	fmt.Printf("name=%v, age=%d, score=%v\n", stu.Name, stu.Age, stu.Score)
}

func (stu *Student) setScore(new_score float64) {
	stu.Score = new_score
}

func (stu *Student) getSum(n1, n2 int) int {
	return n1 + n2
}

type Pupil struct {
	Student // 嵌入匿名结构体
}

type Graduate struct {
	Student
}

func (p *Pupil) testing() {
	fmt.Println("小学生正在考试")
}

func (g *Graduate) testing() {
	fmt.Println("大学生正在考试")
}
