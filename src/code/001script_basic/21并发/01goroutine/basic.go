package main

import (
	"fmt"
	"time"
)

func Basic() {
	for i := 0; i < 100; i++ {
		// go func()() 就是开启一个协程
		go func(n int) {
			fmt.Println("n:", n)
			time.Sleep(time.Second)
		}(i)
	}
	// 主死从随(设置等待2s防止主程序结束，协程被强制关闭)
	time.Sleep(2 * time.Second)
}
