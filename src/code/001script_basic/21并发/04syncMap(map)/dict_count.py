# coding: utf-8
# @Author: cyl
# @File: dict_count.py
# @Time: 2023/06/18 15:34:41
resDict = {}
myList = ["a", "b", "c", "d", "e", "f", "a", "e", "b", "a"]

for value in myList:
    if value in resDict:
        resDict[value] += 1
    else:
        resDict[value] = 1
print(resDict)
