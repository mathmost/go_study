//@Author: cyl
//@File: main.go
//@Time: 2022/06/11 16:41:42
package main

import "sync"

// 单向通道常用于函数的形参上，用于限制通道的写和读: 前读后写
/**
1. ch1 chan<- int: 只能往ch1通道写数据
2. ch2 <-chan int: 只能从ch2通道读数据
*/
var wg sync.WaitGroup

func f1(ch chan<- int) {
	defer wg.Done()
	for i := 0; i < 100; i++ {
		ch <- i
	}
	close(ch)
}

func f2(ch1 <-chan int, ch2 chan<- int) {
	defer wg.Done()
	for {
		// 判断管道是否关闭, 未关闭则取值，反之关闭for循环
		tmp, ok := <-ch1
		if !ok {
			break
		}
		ch2 <- tmp * tmp
	}
	close(ch2)
}

func main() {

}
