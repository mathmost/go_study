//@Author: cyl
//@File: basic.go
//@Time: 2022/06/07 23:04:22
package main

/** channel: 引用类型 */

func notesbook() {
	// var channelName chan T
	// 这段代码表示：通过chan关键字声明了一个新的channel，并且声明时指定channel内传输的数据类型T

	// var a make(chan int) // 不带缓冲的通道初始化
	// var b make(chan string, 10) // 带缓冲的通道初始化

	/** channel的发送和接收、关闭*/
	// channel作为一个队列，他会保证数据接收发的顺序总是遵循先入后出的原则进行；
	// 同时他也会保证同一时刻有且仅有一个goroutinue访问channel来发送和获取数据

	// 发送，如 把10发送到channel(channel <-10)
	// 接收，如 1.从channel中取值并赋值给x(x := <-channel) 2.从channel中取值忽略结果(<-channel)
	// 关闭，如：close(channel)
}
