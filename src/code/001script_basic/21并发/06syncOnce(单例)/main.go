package main

import (
	"sync"
)

/**
Once: 初始化单例资源
使用场景：
1、单例场景
2、仅加载一次的数据，懒加载场景
*/

type singleton struct{}

var instance *singleton
var once sync.Once

// 该实现利用sync.Once类型去同步对GetInstance()的访问，并确保我们的类型仅被初始化一次
func GetInstance() *singleton {
	once.Do(func() {
		instance = &singleton{}
	})
	return instance
}
