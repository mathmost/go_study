//@Author: cyl
//@File: main.go
//@Time: 2022/06/11 14:52:54
package main

import (
	"fmt"
	"runtime"
	"sync"
)

var wg sync.WaitGroup

func a() {
	defer wg.Done()
	for i := 0; i < 10; i++ {
		fmt.Printf("A:%d\n", i)
	}
}

func b() {
	defer wg.Done()
	for i := 0; i < 10; i++ {
		fmt.Printf("B:%d\n", i)
	}
}

func main() {
	// 指定使用系统的内核数量
	// 默认跑满整个cpu的核心数
	runtime.GOMAXPROCS(2)
	fmt.Println("核心数:", runtime.NumCPU()) // 核心数: 8
	wg.Add(2)
	go a()
	go b()
	wg.Wait()
}
