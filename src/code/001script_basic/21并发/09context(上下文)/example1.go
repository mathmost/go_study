//@Author: cyl
//@File: example.go
//@Time: 2023/06/12 23:09:26
package main

import (
	"fmt"
	"sync"
	"time"
)

// 希望启动一个goroutine实时监控cpu的信息, 但是希望可以随时中断
// 第一种方式：通过全局变量控制
var stop bool
var wg sync.WaitGroup

func cpuInfo() {
	defer wg.Done()
	for {
		if stop {
			break
		}
		time.Sleep(2 * time.Second)
		fmt.Println("cpu信息读取完成")
	}
}

func main() {
	wg.Add(1)
	go cpuInfo()
	time.Sleep(time.Second * 6)
	stop = true
	wg.Wait()
	fmt.Println("信息监控结束!")
}
