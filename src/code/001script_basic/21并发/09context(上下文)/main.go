//@Author: cyl
//@File: main.go
//@Time: 2023/06/12 08:14:01
package main

import (
	"context"
	"fmt"
	"sync"
	"time"
)

/** 上下文context */
// 希望启动一个goroutine实时监控cpu的信息, 但是希望可以随时中断
// 第三种方式：通过上下文context
var wg sync.WaitGroup

func cpuInfo(ctx context.Context) {
	defer wg.Done()
	for {
		select {
		case <-ctx.Done():
			fmt.Println("退出cpu监控")
			return
		default:
			time.Sleep(2 * time.Second)
			fmt.Println("cpu信息读取完成")
		}
	}
}

func main() {
	wg.Add(1)
	ctx, cancel := context.WithCancel(context.Background())
	go cpuInfo(ctx)
	time.Sleep(time.Second * 6)
	cancel() // 告诉上下文ctx对象，该结束了
	wg.Wait()
	fmt.Println("信息监控结束!")
}
