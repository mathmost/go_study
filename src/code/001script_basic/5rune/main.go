// @Author: cyl
// @File: main.go
// @Time: 2023/07/03 23:11:15
package main

import (
	"fmt"
)

/**
0、go语言使用utf-8编码
1、在go语言中，字符串主要有每个字符组成，例如hello由h e l l o字符组成，且string的本质就是一个byte数组
2、而在go语言中，字符类型主要有两种：byte rune
3、byte 等价于 uint8 整型对应的ascii字符集编码
	即byte类型对应一个ascii字符，占一个字节8bit，所以byte类型表示的范围为0-255，例如字母a的ascii = 97
4、rune 等价于 int32 整型对应的Unicode字符集编码
	即rune类型代表一个utf-8字符，属于unicode字符集，rune类型占4个字节，即32bit，
	所以rune类型表示的范围相对于byte类型更大，例如字母a的unicode = 97，汉字"你"在unicode编码为2030，
	Unicode字符集是一个可以表示全球范围内绝大多数字符的编码规则，所以当我们想处理中文 日文或其它复合字符时，就需要用rune类型
*/

func main() {
	// 1、string的本质就是一个byte数组
	string1 := "hello"
	string2 := [5]byte{104, 101, 108, 108, 111}
	fmt.Println("string1:", string1)     // hello
	fmt.Printf("string2: %s\n", string2) // hello

	var strs1 = "worlds"
	for _, val := range strs1 {
		fmt.Printf("strs1的每个字:%c的类型:%T, 其值:%v\n", val, val, val)
	}

	// 1.1 range 等同于 rune类型
	var strs2 = "ilove中国"
	for _, val := range strs2 {
		fmt.Printf("range-strs2的每个字:%c的类型:%T, 其值:%v\n", val, val, val)
		/**
		range-strs2的每个字:i的类型:int32, 其值:105
		range-strs2的每个字:l的类型:int32, 其值:108
		range-strs2的每个字:o的类型:int32, 其值:111
		range-strs2的每个字:v的类型:int32, 其值:118
		range-strs2的每个字:e的类型:int32, 其值:101
		range-strs2的每个字:中的类型:int32, 其值:20013
		range-strs2的每个字:国的类型:int32, 其值:22269
		*/
	}

	// 1.2 for循环等同于 byte类型
	for i := 0; i < len(strs2); i++ {
		fmt.Printf("len-strs2的每个字:%c的类型:%T, 其值:%v\n", strs2[i], strs2[i], strs2[i])
		/**
		len-strs2的每个字:i的类型:uint8, 其值:105
		len-strs2的每个字:l的类型:uint8, 其值:108
		len-strs2的每个字:o的类型:uint8, 其值:111
		len-strs2的每个字:v的类型:uint8, 其值:118
		len-strs2的每个字:e的类型:uint8, 其值:101
		len-strs2的每个字:ä的类型:uint8, 其值:228
		len-strs2的每个字:¸的类型:uint8, 其值:184
		len-strs2的每个字:­的类型:uint8, 其值:173
		len-strs2的每个字:å的类型:uint8, 其值:229
		len-strs2的每个字:类型:uint8, 其值:155
		len-strs2的每个字:½的类型:uint8, 其值:189
		*/
	}

	// 2、byte和rune类型申明(只能用单引号，在go语言中，双引号表示字符串，单引号表示字符)
	var a byte = 'A'
	var b rune = 'A'
	var c rune = '中'
	fmt.Println("a:", a)                            // a: 65
	fmt.Println("b:", b)                            // b: 65
	fmt.Println("c:", c)                            // c: 20013
	fmt.Printf("c: 值:%v, 原输出:%c, 类型:%T\n", c, c, c) // 值:20013, 原输出:中, 类型:int32
}
