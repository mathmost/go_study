package main

import (
	"fmt"
	calc "hello/src/code/001script_basic/7package/calc"
	// calc "../calc"   不推荐使用相对路径导入包
)

func init() {
	fmt.Println("main_init()")
}

func main() {
	// 导入包并调用包的方法
	fmt.Println(calc.Add(10, 20))      // 30
	fmt.Println(calc.Sub(10, 20))      // -10
	fmt.Println(calc.Ride(10, 20))     // 200
	fmt.Println(calc.Division(10, 20)) // 0
}
