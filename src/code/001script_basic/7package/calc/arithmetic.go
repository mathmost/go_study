package calc

import "fmt"

func init() {
	fmt.Println("calc_init()")
}

func Add(x, y int) int {
	return x + y
}

func Sub(x, y int) int {
	return x - y
}

func Ride(x, y int) int {
	return x * y
}

func Division(x, y int) int {
	return x / y
}
