package main

import (
	"fmt"
	"os"
)

// 菜单提示信息
func showMenu() {
	fmt.Println("欢迎来到学院管理系统")
	fmt.Println("1. 添加学员")
	fmt.Println("2. 编辑学员")
	fmt.Println("3. 展示学员")
	fmt.Println("4. 退出")
}

// 获取输入信息
func getInput() *student {
	var (
		id    int
		name  string
		class string
	)
	fmt.Println("请按要求输入学生信息")
	fmt.Println("请输入学生ID: ")
	fmt.Scanf("%d ", &id)

	fmt.Println("请输入学生姓名: ")
	fmt.Scanf("%s", &name)

	fmt.Println("请输入学生班级: ")
	fmt.Scanf("%s", &class)

	stu := newStudent(id, name, class)
	return stu
}

func main() {
	sm := newStudentMgr()
	for {
		showMenu()
		// 1. 等待用户输入
		var input int
		fmt.Println("请输入你要操作的序号: ")
		fmt.Scanf("%d\n", &input)
		// 2. 执行用户输入的选项
		switch input {
		case 1:
			sm.addStudent(getInput())
			fmt.Println("添加成功")
		case 2:
			sm.modifyStudent(getInput())
			fmt.Println("编辑成功")
		case 3:
			// 展示所有学员信息
			sm.showStudent()
		case 4:
			os.Exit(0)
			fmt.Println("退出成功")
		}
	}
}
