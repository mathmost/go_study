package main

import "fmt"

type student struct {
	id          int // 学号是唯一的
	name, class string
}

// newStudent 是student的构造函数
func newStudent(id int, name, class string) *student {
	return &student{
		id:    id,
		name:  name,
		class: class,
	}
}

// 学院管理的类型
type studentMgr struct {
	allStudent []*student
}

// newStudent 是studentMgr的构造函数
func newStudentMgr() *studentMgr {
	return &studentMgr{
		allStudent: make([]*student, 0, 100),
	}
}

// 1. 添加学生
func (s *studentMgr) addStudent(newStu *student) {
	s.allStudent = append(s.allStudent, newStu)
}

// 2. 编辑学生
func (s *studentMgr) modifyStudent(newStu *student) {
	for i, v := range s.allStudent {
		if newStu.id == v.id {
			s.allStudent[i] = newStu
			return
		}
	}
	// 学生未找到
	fmt.Printf("输入的信息有误，系统中未找到该学生信息")
}

// 3. 展示学生
func (s *studentMgr) showStudent() {
	for _, v := range s.allStudent {
		fmt.Printf("学号：%d 姓名：%s 班级：%s", v.id, v.name, v.class)
	}
}
