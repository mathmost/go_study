package main

import (
	"fmt"
	"strconv"
	"time"
)

/**
什么是异常(panic)和错误(recover)：
1、异常：不可预知的问题，类似python中的exception
2、错误：可以事先预知到可能出错的可能，把这种情况称为错误，类似：参数检查、数据库无法访问...
*/

func func_error() (int, error) {
	data := "abcd"
	i, err := strconv.Atoi(data)
	return i, err
}

func div(a, b int) (int, error) {
	return a / b, nil
}

func main() {
	/**
	异常：抛出异常(panic) 和 捕捉异常(recover)
	1、panic: 会引起程序直接挂掉
	2、recover: 手动捕捉异常，类似其他语言的try exception
	*/
	a := 20
	b := 0
	defer func() {
		err := recover()
		if err != nil {
			fmt.Println("异常被捕捉到了!")
		}
		fmt.Println("bobby")
	}()
	res, err := div(a, b)
	fmt.Println("res, err:", res, err)

	go func() {
		panic("出错了!...")
	}()
	time.Sleep(10 * time.Second)
}
