package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

// switch case

func bufioDemo() string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("请输入您需要查询该学生的分数: ")
	text, _ := reader.ReadString('\n')
	text = strings.TrimSpace(text)

	return text
}

func switchType(param interface{}) {
	switch param.(type) {
	case string:
		fmt.Println("in type of string")
	case int:
		fmt.Println("in type of int")
	default:
		fmt.Println("in type not found")
	}
}

func main() {
	stuName := bufioDemo()
	// 在go语言中，不需要手动break
	switch stuName {
	case "100":
		fmt.Println("太棒了，这个学生满分！")
	case "80":
		fmt.Println("太棒了，这个学生很优秀！")
	case "60", "59", "58":
		fmt.Println("太棒了，这个学生刚好及格！")
	default:
		fmt.Println("太棒了，这个学生还需要努力！")
	}

	// switch穿透-fallthrough, 如果在case语句块后面增加fallthrough，则会继续执行下一个case, 也叫switch穿透
	// fallthrough只穿透一层case
	// 下面的例子中：比如匹配搭配stuName为100，希望继续执行stuName=80的代码块，则可以在case "100"后增加fallthrough
	switch stuName {
	case "100":
		fmt.Println("太棒了，这个学生满分！")
		fallthrough
	case "80":
		fmt.Println("fallthrough继续执行!")
		fmt.Println("太棒了，这个学生很优秀！")
	case "60":
		fmt.Println("太棒了，这个学生刚好及格！")
	default:
		fmt.Println("太棒了，这个学生还需要努力!")
	}

	// switch判断数据类型
	switchType("hhh")
	switchType(100)
	switchType(100.23)
}
