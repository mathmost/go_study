package main

import "fmt"

// 枚举
// 在go语言中没有枚举类型，但是我们可以使用const + iota(常量累加器)来进行模拟

func main() {
	// 一、模拟一个一周的枚举
	// const属于预编译期赋值, 不需要:=自动推导
	// 如果一行出现两个iota，则这两个iota值是相同的
	// 每个常量组的const中的iota是独立的，如果遇到const,iota会自动清零
	const (
		MONDAY    = iota
		TUESDAY   = iota
		WEDNESDAY = iota
		THURSDAY
		FRIDAY
		STATURDAY
		SUNDAY
	)
	fmt.Println("MONDAY: ", MONDAY)       // MONDAY:  0
	fmt.Println("TUESDAY: ", TUESDAY)     // TUESDAY:  1
	fmt.Println("WEDNESDAY: ", WEDNESDAY) // WEDNESDAY:  2
	fmt.Println("THURSDAY: ", THURSDAY)   // THURSDAY:  3
	fmt.Println("FRIDAY: ", FRIDAY)       // FRIDAY:  4
	fmt.Println("STATURDAY: ", STATURDAY) // STATURDAY:  5
	fmt.Println("SUNDAY: ", SUNDAY)       // SUNDAY:  6

	const (
		a = iota + 1 // 1
		b            // 2
		c            // 3
	)
	fmt.Println("a: ", a) // a:  1
	fmt.Println("b: ", b) // b:  2
	fmt.Println("c: ", c) // c:  3
}
