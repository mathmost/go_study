package main

import "fmt"

// 函数递归

// 求斐波那契数列
func fbnl(num int) int {
	if num <= 1 {
		return num
	}
	return fbnl(num-1) + fbnl(num-2)
}

func main() {
	res := fbnl(9)
	fmt.Println("结果:", res)
}
