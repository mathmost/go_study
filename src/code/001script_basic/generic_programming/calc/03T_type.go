package calc

import "fmt"

// 2. 泛型类型
type User struct {
	ID   int64
	Name string
	Age  int8
}

type Address struct {
	ID       int64
	Province string
	City     string
}

// 集合转列表
func mapToList[k comparable, T any](mp map[k]T) []T {
	list := make([]T, len(mp))
	var i int
	for _, data := range mp {
		list[i] = data
		i++
	}
	return list
}

func MyPrintln[T any](ch chan T) {
	for data := range ch {
		fmt.Println(data)
	}
}

func TTypeCase() {
	userMap := make(map[int64]User, 0)
	userMap[1] = User{ID: 1, Name: "张三", Age: 18}
	userMap[2] = User{ID: 2, Name: "李四", Age: 28}
	userMap[3] = User{ID: 3, Name: "王五", Age: 38}
	userList := mapToList[int64, User](userMap)

	ch := make(chan User)
	go MyPrintln(ch)
	for _, u := range userList {
		ch <- u
	}

	addrMap := make(map[int64]Address, 0)
	addrMap[1] = Address{ID: 1, Province: "湖南", City: "长沙"}
	addrMap[2] = Address{ID: 2, Province: "湖北", City: "武汉"}
	addrMap[3] = Address{ID: 3, Province: "上海", City: "上海"}
	addrList := mapToList[int64, Address](addrMap)

	ch1 := make(chan Address)
	go MyPrintln(ch1)
	for _, addr := range addrList {
		ch1 <- addr
	}
}
