// @Author: cyl
// @File: 04T_map.go
// @Time: 2024/06/25 21:46:57
package calc

import (
	"fmt"
)

// 泛型切片
type List[T any] []T

// 泛型集合: 声明两个泛型, 一个k, 一个v
type MapT[k comparable, v any] map[k]v

// 泛型通道
type MyChan[T any] chan T

type MapUser struct {
	ID   int64
	Name string
	Age  int8
}

type MapAddress struct {
	ID       int64
	Province string
	City     string
}

// 集合转列表
func mapMapToList[k comparable, T any](mp map[k]T) []T {
	list := make([]T, len(mp))
	var i int
	for _, data := range mp {
		list[i] = data
		i++
	}
	return list
}

func mapMyPrintln[T any](ch chan T) {
	for data := range ch {
		fmt.Println(data)
	}
}

func TMapCase() {
	MapUserMap := make(MapT[int, MapUser], 0)
	MapUserMap[1] = MapUser{ID: 1, Name: "张三", Age: 18}
	MapUserMap[2] = MapUser{ID: 2, Name: "李四", Age: 28}
	MapUserMap[3] = MapUser{ID: 3, Name: "王五", Age: 38}

	MapUserList := mapMapToList[int, MapUser](MapUserMap)

	ch := make(MyChan[MapUser])
	go mapMyPrintln(ch)
	for _, u := range MapUserList {
		ch <- u
	}

	addrMap := make(MapT[int, MapAddress], 0)
	addrMap[1] = MapAddress{ID: 1, Province: "湖南", City: "长沙"}
	addrMap[2] = MapAddress{ID: 2, Province: "湖北", City: "武汉"}
	addrMap[3] = MapAddress{ID: 3, Province: "上海", City: "上海"}

	addrList := mapMapToList[int, MapAddress](addrMap)

	ch1 := make(MyChan[MapAddress])
	go mapMyPrintln(ch1)
	for _, addr := range addrList {
		ch1 <- addr
	}
}
