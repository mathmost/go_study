package calc

import "fmt"

type InterfaceUser struct {
	Id   int64
	Name string
	Age  int32
}

type InterfaceAddress struct {
	Id       int64
	Province string
	City     string
}

// 一、普通接口
type ToString interface {
	OldStrinig() string
}

func (u InterfaceUser) OldStrinig() string {
	return fmt.Sprintf("ID: %d, Name: %s, Age: %d", u.Id, u.Name, u.Age)
}

func (addr InterfaceAddress) OldStrinig() string {
	return fmt.Sprintf("ID: %d, Province: %s, City: %s", addr.Id, addr.Province, addr.City)
}

// 二、泛型接口
type GetKey[T comparable] interface {
	any
	Get() T
}

func (u InterfaceUser) Get() int64 {
	return u.Id
}

func (addr InterfaceAddress) Get() string {
	return addr.Province
}
