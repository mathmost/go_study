package calc

import (
	"fmt"
	"reflect"
)

// 1. 泛型函数
func GetMaxNum[T int | float64](a, b T) T {
	if a > b {
		return a
	}
	return b
}

type MyCusNumT interface {
	// 支持uint8 | int32 | float64与int64及其衍生类型
	// ｜ 表示取并集
	// ～ 表示支持类型的衍生类型
	// ps: 多行之间取交集
	uint8 | int32 | float64 | ~int64
}

// MyInt64为int64的衍生类型,是具有基础类型int64的新类型,与int64是不同的类型
type MyInt64 int64

// MyInt32为int32的别名,与int32是同一类型
type MyInt32 = int32

func GetCusNumType[T MyCusNumT](a, b T) T {
	if a > b {
		return a
	}
	return b
}

func GetBuildInComparable[T comparable](a, b T) bool {
	// comparable只支持==和!=两个操作
	return a == b
}

func GetBuildInAny[T any](a, b T) {
	fmt.Println("内置类型any中a, b的类型:", reflect.TypeOf(a), reflect.TypeOf(b))
}
