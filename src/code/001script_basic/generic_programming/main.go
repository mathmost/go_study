// @Author: cyl
// @File: main.go
// @Time: 2024/06/25 21:55:00
package main

import (
	"fmt"
	mycase "hello/src/code/001script_basic/generic_programming/calc"
)

func main() {
	// 1. 显式和隐式调用
	fmt.Println("隐式调用比较1:", mycase.GetMaxNum(10, 20))
	fmt.Println("隐式调用比较2:", mycase.GetMaxNum(50, 60))

	fmt.Println("显示指定传入的类型:", mycase.GetMaxNum[float64](100, 200))
	fmt.Println("显示指定传入的类型:", mycase.GetMaxNum[int](500, 300))

	// 2. 自定义泛型
	var a, b mycase.MyInt32 = 10, 20
	fmt.Println("自定义类型调用:", mycase.GetCusNumType(a, b))

	// 3. 内置类型 comparable
	var c, d string = "abc", "def"
	fmt.Println("内置类型 comparable泛型类型约束 调用:", mycase.GetBuildInComparable(c, d))

	// 4. any类型
	var e, f any = "123", 124.01
	mycase.GetBuildInAny(e, f)

	// 5. 泛型类型
	mycase.TTypeCase()

	// 6. 泛型集合、泛型切片
	mycase.TMapCase()

	// 7. 泛型接口
	mycase.ReceiverCase()
}
