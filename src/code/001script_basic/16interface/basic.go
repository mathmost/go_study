//@Author: cyl
//@File: basic.go
//@Time: 2022/06/10 23:33:31
package main

import (
	"fmt"
)

type animalBasic interface {
	Move()
	Say(content string) string
}

type DogBasic struct {
	Name string
	Feet int
}

type CatBasic struct {
	Name string
	Feet int
}

func (d *DogBasic) Move() {
	fmt.Printf("%s, 狗在动\n", d.Name)
}

func (d *DogBasic) Say(content string) string {
	fmt.Printf("%s, 狗在说: %s\n", d.Name, content)
	return ""
}

func (c *CatBasic) Move() {
	fmt.Printf("%s, 猫在动\n", c.Name)
}

func (c *CatBasic) Say(content string) string {
	fmt.Printf("%s, 猫在说: %s\n", c.Name, content)
	return ""
}
