//@Author: cyl
//@File: duotai.go
//@Time: 2023/06/04 21:33:28
package main

import (
	"fmt"
	"strconv"
)

// 多态

type AttackPlayer interface {
	Attack()
}

type LowHumanLevel struct {
	Name  string
	Level int
}

type HighHumanLevel struct {
	Name  string
	Level int
}

func (a *LowHumanLevel) Attack() {
	fmt.Println("我是", a.Name+", 我的等级是", strconv.Itoa(a.Level)+", 造成了100点伤害")
}

func (a *HighHumanLevel) Attack() {
	fmt.Println("我是", a.Name+", 我的等级是", strconv.Itoa(a.Level)+", 造成了5000点伤害")
}

func DoAttack(a AttackPlayer) {
	a.Attack()
}
