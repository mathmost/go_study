//@Author: cyl
//@File: empty.go
//@Time: 2023/06/05 21:32:48
package main

import (
	"fmt"
)

/**
1、空接口类似于java和python中的object
2、空接口的第一个用途：可以把任意类型赋值给空接口变量
3、空接口的第二个用途：参数传递
4、空接口的第三个用途：空接口可以作为map的值
*/

type Dog struct {
	Name string
	age  uint8
}

// 1、定义空接口方式1
type EmptyInterfacer interface{}

func EmptyInterfaceType2() {
	// 定义空接口方式2
	var i interface{} = Dog{}
	fmt.Println("i:", i)
}

// 2、参数传递
func emptyInterfaceJustifyType(x interface{}) {
	switch v := x.(type) {
	case string:
		fmt.Printf("x is a string, value is %v\n", v)
	case int:
		fmt.Printf("x is a int is %v\n", v)
	case bool:
		fmt.Printf("x is a bool is %v\n", v)
	default:
		fmt.Println("unsupport type!")
	}
}

// 3、空接口可以作为map的值
func emptyInterfaceMapValue() {
	var teacherMap = make(map[string]interface{})
	teacherMap["name"] = "lucy"
	teacherMap["age"] = 18
	teacherMap["height"] = 75.2
	teacherMap["courses"] = []string{"django", "sanic", "flask"}
	fmt.Printf("teacherMap:%T\n", teacherMap)
}
