package main

import (
	"fmt"
	"unicode"
)

/** str and goto use*/

func nineAndNine() {
	var num = 10
	for i := 1; i < num; i++ {
		for j := 1; j <= i; j++ {
			fmt.Printf(" %d * %d = %d\t", j, i, i*j)
		}
		fmt.Println()
	}
}

func gotoDemo() {
	for i := 0; i < 10; i++ {
		for j := 0; j < 10; j++ {
			if j == 2 {
				// 设置退出标签
				goto breakTag
			}
			fmt.Printf("%v-%v\n", i, j) // 0-0 0-1
		}
	}
	// 手动return，避免执行进入标签...
	return
	// 标签
breakTag:
	fmt.Println("结束for循环")
}

func main() {
	// 1. 编写代码分别定义一个整型、浮点型、布尔型、字符串型变量, 并分别打印出上述变量的值和类型
	var (
		a int
		b float32
		c bool
		d string
	)
	fmt.Printf("%T, %d\n", a, a) // int, 0
	fmt.Printf("%T, %f\n", b, b) // float32, 0.000000
	fmt.Printf("%T, %t\n", c, c) // bool, false
	fmt.Printf("%T, %s\n", d, d) // string, (None)

	var my_str = "小王子"
	fmt.Println("my str len: ", len(my_str)) // my str len:  9

	var my_pj = "真帅!"
	fmt.Println("my str pingjie: ", my_str+my_pj) // my str pingjie: 小王子真帅!

	// 2. 编写代码统计出字符串"hello沙河小王子"中汉字的数量
	var (
		strs   string = "hello沙河小王子, 我是沙河娜扎!"
		_count int    = 0
	)
	for _, char := range strs {
		if unicode.Is(unicode.Han, char) {
			_count++
		}
	}
	fmt.Printf("该字符串汉字总数为: %d个\n", _count)

	// 3. 九九乘法表
	nineAndNine()

	// 4. goto
	gotoDemo()
}
