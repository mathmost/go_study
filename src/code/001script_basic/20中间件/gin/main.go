package main

import (
	"log"
	"time"

	"github.com/gin-gonic/gin"
)

// 中间件
// gin框架规定: gin的中间件类型为gin.HandlerFunc

func Logger() gin.HandlerFunc {
	return func(c *gin.Context) {
		t := time.Now()
		// 1. 设置上下文变量
		c.Set("example", "123456")
		// 2. before request
		log.Println("中间件1执行前: 123")
		c.Next()

		// 3. after request
		latency := time.Since(t)
		log.Println("中间件1执行时间: ", latency)
		// 当获取到数据时, 接收数据并打印
		status := c.Writer.Status()
		log.Println("中间件1响应状态: ", status)
	}
}

func TestMiddleWare() gin.HandlerFunc {
	return func(c *gin.Context) {
		t := time.Now()
		// before request
		log.Println("中间件2执行前: 456")
		c.Next()
		// after request
		latency := time.Since(t)
		log.Println("中间件2执行时间: ", latency)
		status := c.Writer.Status()
		log.Println("中间件2响应状态: ", status)
	}
}

func main() {
	// 初始化引擎
	router := gin.New()
	// 使用中间件
	/*
		1. 中间件为栈结构, 先进后出: 比如下面先使用了Logger(), 后使用了TestMiddleWare()
		2. 整个路由的调用过程为:
		3. Logger() before -> TestMiddleWare() before -> 路由 -> TestMiddleWare() after -> Logger() after
	*/
	router.Use(Logger())
	router.Use(TestMiddleWare())
	// 注册路由
	router.GET("/test", func(c *gin.Context) {
		example := c.MustGet("example")
		log.Println("路由: ", example)
	})

	// 运行
	router.Run(":9999")
}
