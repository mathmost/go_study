package main

import _case "hello/src/code/001script_basic/17interfaceImpl/case"

/** interface隐式实现 */

func animalLife(a _case.Animal) {
	a.Say()
	a.Speak()
	a.Eating()
	a.Running()
	a.Deathing()
}

func main() {
	catMain := _case.NewCat()
	animalLife(catMain)

	DogMain := _case.NewDog()
	animalLife(DogMain)
}
