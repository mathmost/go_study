package _case

import "fmt"

type Animal interface {
	Say() string
	Speak() string
	Eating() string
	Running() string
	Deathing()
}

type animal struct{}

func (a animal) Say() string {
	return "Animal Saying..."
}

func (a animal) Speak() string {
	return "Animal Speaking..."
}

func (a *animal) Eating() string {
	return "Animal Eating..."
}

func (a animal) Running() string {
	return "Animal Running..."
}

func (a animal) Deathing() {
	fmt.Println("Animal Deathing...")
}

// func init() {
// 	var ani = &animal{}
// 	ani.Say()
// 	ani.Speak()
// 	ani.Eating()
// 	ani.Running()
// 	ani.Deathing()
// }
