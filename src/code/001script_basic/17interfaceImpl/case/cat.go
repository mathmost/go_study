package _case

import "fmt"

type Cat struct {
	// 直接继承animal的实现，也就是所谓的隐式实现
	animal
}

func NewCat() Animal {
	return &Cat{}
}

// 重写cat的Deathing方法
func (c *Cat) Deathing() {
	fmt.Println("Cat Deathing...")
}
