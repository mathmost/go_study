package _case

import "fmt"

type Dog struct {
	// 直接继承animal的实现，也就是所谓的隐式实现
	animal
}

func NewDog() Animal {
	return &Dog{}
}

func (d Dog) Say() string {
	return "Dog Saying..."
}

func (d Dog) Speak() string {
	return "Dog Speaking..."
}

func (d *Dog) Eating() string {
	return "Dog Eating..."
}

func (d Dog) Running() string {
	return "Dog Running..."
}

func (d Dog) Deathing() {
	fmt.Println("Dog Deathing...")
}
