package main

import "fmt"

// 指针逃逸

// https://books.studygolang.com/GoExpertProgramming/chapter04/4.3-escape_analysis.html

type Student struct {
	Name string
	Age int
}

// 指针逃逸一原理：是一个是因为new返回的指针 指向的堆 当局部变量的栈指针没有被分配的时候 就会出现逃逸到堆上面
func StudentRegister(name string, age int) *Student {
	s := new(Student) // 局部变量s逃逸到堆

	s.Name = name
	s.Age = age

	return s
}

// 指针逃逸二原理：当局部变量不是new出来的，而是一个变量，那这个变量就会压到栈上，当随着扩容的时候，栈发生了拷贝，引用的地址发生了改变，也会出现内存逃逸
func escape() *int {
	j := 10
	fmt.Println("j:",j)
	return &j
}

func main() {
	// 指针逃逸一
	for i := 0; i < 1000; i++ {
		StudentRegister("Jim", 18)
	}

	// 指针逃逸二
	a := escape()
	println("a:",a) // a: 0xc000014088
}
