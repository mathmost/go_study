package main

import (
	"bytes"
	"fmt"
	"strconv"
	"strings"
)

// strings

func Join(str []string, sep string) string {
	if len(str) == 0 {
		return ""
	}
	if len(str) == 1 {
		return str[0]
	}
	buffer := bytes.NewBufferString(str[0])
	for _, s := range str[1:] {
		buffer.WriteString(sep)
		buffer.WriteString(s)
	}
	return buffer.String()
}

func main() {
	// 字符串比较
	a, b  := "gopher", "hello"
	// Compare 函数，用于比较两个字符串的大小，如果两个字符串相等，返回为 0。如果 a 小于 b ，返回 -1 ，反之返回 1 
	// 不推荐使用这个函数，直接使用 == != > < >= <= 等一系列运算符更加直观
	println(strings.Compare(a, b)) // -1

	// EqualFold 函数，计算 a 与 b 忽略字母大小写后是否相等
	println(strings.EqualFold(a, b)) // false
	println(strings.EqualFold("GO", "go")) // true

	// 是否存在某个字符或子串
	println(strings.ContainsAny("team", "i")) // false
	println(strings.ContainsAny("user", "u & r")) // true

	// 子串出现次数 ( 字符串匹配 )
	println(strings.Count("cheese", "e")) // 3
	println(len("谷歌中国")) // 12

	// split
	fmt.Printf("%q\n", strings.Split("foo,bar,baz", ",")) // ["foo" "bar" "baz"]

	// 判断前缀
	fmt.Println(strings.HasPrefix("Gopher", "Go"))
	fmt.Println(strings.HasPrefix("Gopher", "C"))
	fmt.Println(strings.HasPrefix("Gopher", ""))

	// 判断后缀
	fmt.Println(strings.HasSuffix("Amigo", "go"))
	fmt.Println(strings.HasSuffix("Amigo", "Ami"))
	fmt.Println(strings.HasSuffix("Amigo", ""))
	
	// Join
	fmt.Println(Join([]string{"name=hello", "age=小王子"}, "&")) // name=hello&age=小王子

	// 字符串重复几次
	fmt.Println("ba" + strings.Repeat("na", 2)) // banana

	// 字符串替换
	fmt.Println(strings.Replace("oink oink oink", "k", "ky", 2))      // oinky oinky oink
	fmt.Println(strings.Replace("oink oink oink", "oink", "moo", -1)) // moo moo moo
	fmt.Println(strings.ReplaceAll("oink oink oink", "oink", "moo"))  // moo moo moo

	// 大小写转换
	fmt.Println(strings.ToLower("HELLO, WORLD!")) // hello, world!
	fmt.Println(strings.ToUpper("hello, world!")) // HELLO, WORLD!

	// 数据类型转换
	n, err := strconv.ParseInt("128", 10, 8)
	if err != nil {
		fmt.Println("err:",err) // strconv.ParseInt: parsing "128": value out of range
	}
	fmt.Println(n) // 127
}
