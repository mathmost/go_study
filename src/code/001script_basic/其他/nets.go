package main

import (
	"fmt"
	"net"
)

// net网络请求

func main() {
	// 1. 读取网络接口的配置文件
	// 函数net.Interfaces()的作用是将当前计算机的所有接口信息通过一个切片数据结构返回，切片的数据元素类型为net.Interface。此切片将用于获取接口的信息
	interfaces, err := net.Interfaces()
    if err != nil {
        fmt.Println(err)
        return
    }
	// interfaces [{11 1500 以太网 4c:ed:fb:3d:64:16 up|broadcast|multicast} {1 -1 Loopback Pseudo-Interface 1  up|loopback|multicast}]
	fmt.Println("interfaces", interfaces)

	for _, i := range interfaces {
		fmt.Printf("Interface: %v\n", i.Name)
        ByName, err := net.InterfaceByName(i.Name)
        if err != nil {
            fmt.Println(err)
        }
		fmt.Println("ByName:", ByName)
	}
}
