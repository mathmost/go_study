package main

import (
	"fmt"
	"strconv"
)

// strconv: 实现了基本数据类型和其字符串表示的相互转换

func main() {
	/*
		一、string和int类型转换
	*/
	// Atoi(): 用于将字符串类型的整数转换为int类型; 如果传入的字符串参数无法转换为int类型，就会返回错误
	s1, err := strconv.Atoi("100")
	if err != nil {
		fmt.Println("err:", err)
	}
	fmt.Println("s1:", s1) // 100

	// Itoa(): 函数用于将int类型数据转换为对应的字符串表示
	s2 := strconv.Itoa(100)
	fmt.Println("s2:", s2) // "100"

	/*
		二、parse系列函数(Parse类函数用于转换字符串为给定类型的值：ParseBool()、ParseFloat()、ParseInt()、ParseUint())
	*/
	// ParseBool(): 返回字符串表示的bool值, 它接受1、0、t、f、T、F、true、false、True、False、TRUE、FALSE；否则返回错误
	s3, _ := strconv.ParseBool("true")

	// ParseInt(): 返回字符串表示的整数值，接受正负号
	s4, _ := strconv.ParseInt("-2", 10, 64)

	// ParseUnit(): ParseUint类似ParseInt但不接受正负号，用于无符号整型
	s5, _ := strconv.ParseUint("2", 10, 64)

	// ParseFloat(): 解析一个表示浮点数的字符串并返回其值
	s6, _ := strconv.ParseFloat("3.1415", 64)
	fmt.Println("s3:", s3) // true
	fmt.Println("s4:", s4) // -2
	fmt.Println("s5:", s5) // 2
	fmt.Println("s6:", s6) // 3.1415

	/*
		三、format系列函数: Format系列函数实现了将给定类型数据格式化为string类型数据的功能
	*/
	// FormatBool(): 根据b的值返回”true”或”false”
	s7 := strconv.FormatBool(true)

	// FormatInt(): 返回i的base进制的字符串表示。base 必须在2到36之间，结果中会使用小写字母’a’到’z’表示大于10的数字
	s8 := strconv.FormatFloat(3.1415, 'E', -1, 64)

	// FormatUnit(): 是FormatInt的无符号整数版本
	s9 := strconv.FormatInt(-2, 16)

	// FormatFloat(): 函数将浮点数表示为字符串并返回
	s10 := strconv.FormatUint(2, 16)
	fmt.Println("s7:", s7)   // true
	fmt.Println("s8:", s8)   // 3.1415E+00
	fmt.Println("s9:", s9)   // -2
	fmt.Println("s10:", s10) // 2

	/*
		四、其他
	*/
	// isPrint(): 返回一个字符是否是可打印的，和unicode.IsPrint一样，r必须是：字母（广义）、数字、标点、符号、ASCII空格
	// rune: int32的别名（-2的31次方~2的31次方-1），用来区分字符值和整数值
	r := rune(0)
	fmt.Println("isPrint:", strconv.IsPrint(r)) // false

	// canBackquote(): 返回字符串s是否可以不被修改的表示为一个单行的、没有空格和tab之外控制字符的反引号字符串
	s12 := "樱桃小丸子"
	fmt.Println("canBackquote:", strconv.CanBackquote(s12)) // true

	// strconv包中还有Append系列、Quote系列等函数
}
