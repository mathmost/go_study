package main

import "fmt"

// 短变量声明

func fun1() {
	i := 0
	i, j := 1, 2
	fmt.Printf("i = %d, j = %d\n", i, j) // i = 1, j = 2
}

// func fun2(i int) {
//     i := 0
//     fmt.Println(i)
// }

func fun3() {
	i, j := 0, 0
	if true {
		j, k := 1, 1
		fmt.Printf("j = %d, k = %d\n", j, k) // j = 1, k = 1
	}
	fmt.Printf("i = %d, j = %d\n", i, j) // i = 0, j = 0
}

func main() {
	// 一
	fun1()

	// 二
	// fun2() 无法通过编译

	// 三
	fun3()
}
