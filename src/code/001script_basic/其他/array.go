package main

import (
	"bufio"
	"fmt"
	"os"
)

// 标准输入读取
func bufioReader() {
	inputReader := bufio.NewReader(os.Stdin)
	fmt.Printf("please input youre name: ")
	input, err := inputReader.ReadString('\n')
	if err != nil {
		fmt.Printf("err: %s\n", err)
	} else {
		input = input[:len(input)-1]
		fmt.Println("hello, ", input)
	}
}

// 数组
func basic() {
	var ips = []string{"192.168.1.1", "192.168.1.2", "192.168.1.3"}
	ips = append(ips, "192.168.1.4")
	fmt.Println(ips) // [192.168.1.1 192.168.1.2 192.168.1.3 192.168.1.4]
}

func main() {
	// bufioReader()
	basic()
}
