package main

import "fmt"

type Lunch interface {
	Cook()
}

type Rise struct{}

func (r *Rise) Cook() {
	fmt.Println("it is rise...")
}

type Tomato struct{}

func (t *Tomato) Cook() {
	fmt.Println("it is tomato...")
}

type LunchFactory interface {
	CreteaFood() Lunch
	CreataVegetable() Lunch
}

type SimpleLunchFactory struct{}

func NewSimpleLunchFactory() LunchFactory {
	return &SimpleLunchFactory{}
}

func (s *SimpleLunchFactory) CreteaFood() Lunch {
	return &Rise{}
}

func (s *SimpleLunchFactory) CreataVegetable() Lunch {
	return &Tomato{}
}

// 抽象工厂模式测试
func TestAbstractFactory() {
	factory := NewSimpleLunchFactory()
	food := factory.CreteaFood()
	food.Cook()

	vegetable := factory.CreataVegetable()
	vegetable.Cook()
}
