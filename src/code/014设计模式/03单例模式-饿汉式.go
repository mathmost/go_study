package main

/*
*
饿汉方式
解释：指的是全局单例实例在类装载时构建
缺点：如果Singleton创建初始化比较复杂耗时时，加载时间会延长
*/
type Singleton struct {
}

var ins *Singleton = &Singleton{}

func GetInstance() *Singleton {
	return ins
}
