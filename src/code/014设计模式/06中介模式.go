package main

import "fmt"

type Mediator interface {
	Communicate(who string)
}

type WildStallion interface {
	SetMediator(mediator Mediator)
}

// 客户1
type Bill struct {
	mediator Mediator
}

func (b Bill) SetMediator(mediator Mediator) {
	b.mediator = mediator
}

func (b *Bill) Respond() {
	fmt.Println("bill what ?")
	b.mediator.Communicate("bill")
}

// 客户2
type Ted struct {
	mediator Mediator
}

func (t *Ted) SetMediator(mediator Mediator) {
	t.mediator = mediator
}

func (t *Ted) Talk() {
	fmt.Println("ted: bill?")
	t.mediator.Communicate("ted")
}

func (t *Ted) Respond() {
	fmt.Println("ted: how are you today?")
}

// 客户1、2
type ConcreteMediator struct {
	Bill
	Ted
}

func NewMediator() *ConcreteMediator {
	mediator := &ConcreteMediator{}
	mediator.Bill.SetMediator(mediator)
	mediator.Ted.SetMediator(mediator)
	return mediator
}

func (m *ConcreteMediator) Communicate(who string) {
	if who == "Ted" {
		m.Bill.Respond()
	} else {
		m.Ted.Respond()
	}
}

// 测试中介模式
func TestMediator() {
	mediator := NewMediator()
	mediator.Ted.Talk()
}
