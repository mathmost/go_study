package main

import "fmt"

type Reataurant interface {
	GetFood()
}

type DongLaiShun struct{}

func (dsl *DongLaiShun) GetFood() {
	fmt.Println("东来顺的饭菜已经准备就绪...")
}

type QingFeng struct{}

func (qf *QingFeng) GetFood() {
	fmt.Println("庆丰包子铺...")
}

func NewReataurantFactory(name string) Reataurant {
	switch name {
	case "d":
		return &DongLaiShun{}
	case "q":
		return &QingFeng{}
	}
	return nil
}
