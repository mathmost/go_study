package main

import "sync"

/**
sync.Once
官方描述：Once is an object that will perform exactly one action.（Once是一个对象，保证某个动作只被执行一次，最典型的就是单例模式了）
*/

type Singleton struct {
	num int
}

var ins *Singleton
var once sync.Once

func GetInstance() *Singleton {
	once.Do(func() {
		ins = &Singleton{}
	})
	return ins
}
