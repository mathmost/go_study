package main

/*
*
懒汉方式
解释：指的是全局单例实例在第一次被使用时构建
缺点：非线程安全；当正在创建的时候，如果有线程访问此时的ins=nil就会在创建，单例就会实现多个实例了
*/
type Singleton struct {
}

var ins *Singleton

func GetInstance() *Singleton {
	if ins == nil {
		ins = &Singleton{}
	}
	return ins
}
