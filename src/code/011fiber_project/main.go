package main

import "github.com/gofiber/fiber/v2"

func IndexCallback(c *fiber.Ctx) error {
	return c.SendString("golang技术栈官网-老郭 ?!")
}

func main() {
	app := fiber.New()

	app.Get("/", IndexCallback)
	app.Listen(":3000")
}
