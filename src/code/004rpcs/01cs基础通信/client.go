package main

import (
	"fmt"
	"net/rpc/jsonrpc"
)

// 服务端

func main() {
	// 1. 用 rpc 链接服务器
	// conn, err := rpc.Dial("tcp", "127.0.0.1:8800")  // 使用net/rpc
	conn, err := jsonrpc.Dial("tcp", "127.0.0.1:8800") // 使用net/jsonrpc
	if err != nil {
		fmt.Println("客户端链接服务器失败", err)
		return
	}
	defer conn.Close()

	// 2. 调用远程函数
	var resp string
	if err := conn.Call("hello.HelloWorld", "李白", &resp); err != nil {
		fmt.Println("客户端调用远程函数失败", err)
		return
	}
	fmt.Println(resp)
}
