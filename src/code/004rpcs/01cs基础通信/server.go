package main

import (
	"fmt"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
)

// 客户端

// 定义类对象
type World struct{}

// 绑定类方法
func (this *World) HelloWorld(name string, resp *string) error {
	*resp = name + ", 你好!"
	return nil
}

func main() {
	// 1. 注册rpc服务
	if err := rpc.RegisterName("hello", new(World)); err != nil {
		fmt.Println("注册rpc服务失败", err)
		return
	}

	// 2. 设置监听
	listener, err := net.Listen("tcp", "127.0.0.1:8800")
	if err != nil {
		fmt.Println("设置监听失败", err)
		return
	}
	defer listener.Close()
	fmt.Println("开始监听...")

	// 3. 建立链接
	conn, err := listener.Accept()
	if err != nil {
		fmt.Println("建立链接失败", err)
		return
	}
	defer conn.Close()

	// 4. 绑定服务
	// rpc.ServeConn(conn)  // 使用net/rpc
	jsonrpc.ServeConn(conn) // 使用net/jsonrpc
}
