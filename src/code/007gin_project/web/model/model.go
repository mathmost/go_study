package model

import (
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
	"github.com/spf13/viper"
)

// 数据库建表

// table = user 用户信息
type User struct {
	ID            int           // 用户编号
	Name          string        `gorm:"size:32;unique"` // 用户名
	Password_hash string        `gorm:"size:128"`       // 用户加密后的密码
	Mobile        string        `gorm:"size:11;unique"` // 手机号
	Real_name     string        `gorm:"size:32"`        // 真实姓名
	Id_card       string        `gorm:"size:20"`        // 身份证号
	Avatar_url    string        `gorm:"size:256"`       // 用户头像路径, 通过fastdfs进行图片存储
	Houses        []*House      // 用户发户的房屋信息(一人多套房)
	Orders        []*OrderHouse // 用户订单(一人多次订单)
}

// table = house 房屋信息
type House struct {
	gorm.Model                     // 房屋编号
	UserId          uint           // 房屋主人的用户编号，与用户表关联
	AreaId          uint           // 归属地区的区域编号 和地区表关联
	Title           string         `gorm:"size:64"`                                      // 房屋标题
	Address         string         `gorm:"size:512"`                                     // 地址
	Room_count      int            `gorm:"default:1"`                                    // 房间数目
	Acreage         int            `gorm:"default:0" json:"acreage"`                     // 房屋总面积
	Price           int            `json:"price"`                                        // 价格
	Unit            string         `gorm:"default:1" json:"unit"`                        // 单位, 比如"几室几厅"
	Capacity        int            `gorm:"default:1" json:"capacity"`                    // 房屋容纳总人数
	Beds            string         `gorm:"size:64;default:''" json:"beds"`               // 房屋床铺的配置
	Deposit         int            `gorm:"default:0" json:"deposit"`                     // 押金
	Min_days        int            `gorm:"default:1" json:"min_days"`                    // 最少入住天数
	Max_days        int            `gorm:"default:0" json:"max_days"`                    // 最大入住天数 0表示不限制
	Order_count     int            `gorm:"default:0" json:"order_count"`                 // 预定完成的该房屋的订单数
	Index_image_url string         `gorm:"size:256;default:''" json:"index_image_url"`   // 房屋主图片路径
	Facilities      []*Facility    `gorm:"many2many:house_facilities" json:"facilities"` // 房屋设施 与设施表关联
	Images          []*HosueImaghe `json:"image_urls"`                                   // 房屋图片 除主要图片之外的其他图片地址
	Orders          []*OrderHouse  `json:"orders"`                                       // 房屋订单 与房屋表关联
}

// table = area 区域信息
type Area struct {
	Id     int      `json:"aid"`                 // 区域编号  1   2
	Name   string   `gorm:"size:32" json:"name"` // 区域名字 昌平 海淀
	Houses []*House `json:"houses"`              // 区域内所有的房屋 与房屋表关联
}

// table = facility 设施信息
type Facility struct {
	Id     int      `json:"fid"`     // 设施编号
	Name   string   `gorm:"size:32"` // 设施名字
	Houses []*House `json:"houses"`  // 房屋的所有设施 与房屋表关联
}

// table = house_image 房屋图片
type HosueImaghe struct {
	Id      int    `json:"house_image_id"`      // 图片id
	Url     string `gorm:"size:256" json:"url"` // 图片url, 用于存放房屋的图片
	HosueId uint   `json:"house_id"`            // 图片所属房屋编号
}

// table = orderHouse 订单
type OrderHouse struct {
	gorm.Model            // 订单编号
	UserId      uint      `json:""`              // 下单的用户编号
	HosueId     uint      `json:""`              // 预定的房间编号
	Begin_date  time.Time `gorm:"type:datetime"` // 预定的起始时间
	End_date    time.Time `gorm:"type:datetime"` // 预定的结束时间
	Days        int       // 预定总天数
	House_price int       // 房屋的单价
	Amount      int       // 订单总金额
	Status      string    `gorm:"default:'WAIT_ACCEPT'"` // 订单状态
	Comment     string    `gorm:"size:512"`              // 订单评论
	Credit      bool      // 表示个人征信情况 true表示良好
}

// 创建全局的连接池句柄
var GlobalConn *gorm.DB

func InitDb() (*gorm.DB, error) {
	// 第一种：使用配置文件读取
	driverName := viper.GetString("datasource.driverName")
	host := viper.GetString("datasource.host")
	port := viper.GetString("datasource.port")
	database := viper.GetString("datasource.database")
	username := viper.GetString("datasource.username")
	password := viper.GetString("datasource.password")
	charset := viper.GetString("datasource.charset")
	loc := viper.GetString("datasource.loc")
	args := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=True&loc=%s",
		username,
		password,
		host,
		port,
		database,
		charset,
		loc)
	db, err := gorm.Open(driverName, args)

	// 第二种：手写
	// db, err := gorm.Open("mysql", "root:test123456@tcp(127.0.0.1:3306)/golang_gin_test?charset=utf8&parseTime=True&loc=Asia%2FShanghai")

	// 返回db conn
	if err == nil {
		// 初始化全局的连接池
		GlobalConn = db
		GlobalConn.DB().SetMaxIdleConns(10)
		GlobalConn.DB().SetConnMaxLifetime(100)

		GlobalConn.SingularTable(true)
		GlobalConn.AutoMigrate(new(User), new(House), new(Area), new(Facility), new(HosueImaghe), new(OrderHouse))
		return GlobalConn, nil
	}
	return nil, err
}
