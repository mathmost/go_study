package utils

import (
	"context"
	"fmt"

	"github.com/qiniu/go-sdk/v7/auth/qbox"
	"github.com/qiniu/go-sdk/v7/storage"
)

// 七牛云上传

// 1. 七牛云密钥以及上传的空间名称
var (
	ACCESS_KEY = ""
	SECRET_KEY = ""
	BUCKET     = "golang-gin-project"
)

// 2. 配置
func config() (cfg storage.Config) {
	cfg = storage.Config{}
	// 空间对应机房
	// 其中关于Zone对象和机房的关系如下：
	//    机房    Zone对象
	//    华东    storage.ZoneHuadong
	//    华北    storage.ZoneHuabei
	//    华南    storage.ZoneHuanan
	//    北美    storage.ZoneBeimei
	// 七牛云存储空间设置首页有存储区域
	cfg.Zone = &storage.ZoneHuanan
	// 是否使用https
	cfg.UseHTTPS = false
	// 是否使用CDN上传加速
	cfg.UseCdnDomains = false

	return
}

// 3. upload
func UploadImage(filepath, filename string) {
	// filepath: 设置上传文件
	key := filename // 上传的文件名称
	putPolicy := storage.PutPolicy{
		Scope: BUCKET,
		// ForceSaveKey: true,
	}
	mac := qbox.NewMac(ACCESS_KEY, SECRET_KEY)
	upToken := putPolicy.UploadToken(mac)

	cfg := config()
	// 构建表单上传的对象
	formUploader := storage.NewFormUploader(&cfg)
	ret := storage.PutRet{}

	// 可选配置
	putExtra := storage.PutExtra{
		Params: map[string]string{
			"x:name": "github logo",
		},
	}
	err := formUploader.PutFile(context.Background(), &ret, upToken, key, filepath, &putExtra)
	if err != nil {
		return
	}
	fmt.Printf("上传文件:%s至七牛云成功, hash值为%s!\n", ret.Key, ret.Hash)
}

// 4. 七牛云访问域名: http://qx4wgqjln.hn-bkt.clouddn.com/abc.png
