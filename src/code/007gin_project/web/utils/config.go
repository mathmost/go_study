package utils

const (
	QiniuUrl = "http://qx4wgqjln.hn-bkt.clouddn.com/" // 七牛上传图片url
)

const (
	MYSQL_USERNAME  = "root"            // 用户名
	MYSQL_PASSWORD  = "test123456"      // 密码
	MYSQL_AGREEMENT = "tcp"             // 协议
	MYSQL_URL       = "127.0.0.1:3306"  // IP:PORT
	MYSQL_LIBRARY   = "golang_gin_test" // 库
	MYSQL_CHARSET   = "utf8"            // 编码
	MYSQL_PARSETIME = "True"            // 时间格式
	MYSQL_LOC       = "Asia%2FShanghai" // 时区(解决mysql八小时问题)
)
