package utils

import (
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

// 过滤器

// 登陆中间件
func LoginFilter() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		s := sessions.Default(ctx)
		userName := s.Get("userName")
		if userName == nil {
			var resp = make(map[string]interface{})
			resp["errno"] = RECODE_SESSIONERR
			resp["errmsg"] = RecodeText(RECODE_SESSIONERR)
			ctx.JSON(http.StatusOK, resp)
			ctx.Abort() // 如果获取到的userName为空, 则直接返回不继续执行
		} else {
			ctx.Next()
		}
	}
}
