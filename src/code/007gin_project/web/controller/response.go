package controller

import (
	"hello/src/code/007gin_project/web/utils"
	"strconv"

	"github.com/gin-gonic/gin"
)

// 执行相同的响应代码, 传入resp: map对象
func OperateCommonResp(_resp map[string]interface{}, _error string, _data interface{}) {
	// _resp: map对象
	// _error: 错误码
	// _data: 返回的数据
	_resp["errno"] = _error
	_resp["errmsg"] = utils.RecodeText(_error)
	if _data != nil {
		_resp["data"] = _data
	}
}

// 返回响应数据
func JsonCommonResponse(_error string, data interface{}) map[string]interface{} {
	var resp = make(map[string]interface{})
	resp["errno"] = _error
	resp["errmsg"] = utils.RecodeText(_error)
	if data != nil {
		resp["data"] = data
	}
	return resp
}

func JsonCommonResponse2(ctx *gin.Context, _error int, data interface{}) {
	var resp = make(map[string]interface{})
	resp["errno"] = _error
	statusCode := strconv.Itoa(_error)
	resp["errmsg"] = utils.RecodeText(statusCode)
	if data != nil {
		resp["data"] = data
	}
	ctx.JSON(
		_error,
		resp,
	)
}
