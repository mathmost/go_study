package controller

import (
	"fmt"

	"github.com/gin-gonic/gin"
)

// 房屋模块

// 搜索房源
func GetHouses(ctx *gin.Context) {
	fmt.Println()
}

// 获取用户已发布的房源
func GetUserHouses(ctx *gin.Context) {
	fmt.Println()
}

// 发布房源
func PostHouses(ctx *gin.Context) {
	fmt.Println()
}

// 添加房源图片
func PostHousesImage(ctx *gin.Context) {
	fmt.Println()
}

// 展示房屋详情
func GetHouseInfo(ctx *gin.Context) {
	fmt.Println()
}

// 展示首页轮播图
func GetHouseIndex(ctx *gin.Context) {
	fmt.Println()
}
