package controller

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// 首页

func GetIndex(ctx *gin.Context) {
	JsonCommonResponse2(ctx, http.StatusOK, "index1...")
}

func GetIndex2(ctx *gin.Context) {
	JsonCommonResponse2(ctx, http.StatusOK, "index2...")
}
