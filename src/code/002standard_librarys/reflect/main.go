//@Author: cyl
//@File: main.go
//@Time: 2022/06/19 20:29:02
package main

import (
	"fmt"
	"reflect"
)

/**  反射
1、动态获取和修改字段值
2、动态创建对象 reflect.New
3、动态调用方法 reflect.Value Call
4、动态获取和修改类型信息 包名 类型名称 字段和方法信息 Type获取类型信息
5、解析和调用未知类型的函数 reflect.FuncOf Call调用
*/

type Person struct {
	Name string
	Age  int
}

func main() {
	/*
		1. 类型: reflect.TypeOf()
		2. 值: reflect.ValueOf()
	*/
	var x float64 = 3.1415926

	// 直接获取
	fmt.Println("x type:", reflect.TypeOf(x))   // float64
	fmt.Println("x value:", reflect.ValueOf(x)) // 3.1415926

	// 反射获取: reflect.ValueOf(x)
	v := reflect.ValueOf(x)
	fmt.Println("v kind:", v.Kind() == reflect.Float64) // true
	fmt.Println("v type:", v.Type())                    // float64
	fmt.Println("v value:", v.Float())                  // 3.1415926

	// 动态创建对象
	p := reflect.New(reflect.TypeOf(Person{})).Elem().Addr().Interface().(*Person)
	// 动态设置字段值
	pValue := reflect.ValueOf(p).Elem()
	pValue.FieldByName("Name").SetString("Alice")
	pValue.FieldByName("Age").SetInt(18)
	// 动态获取字段值
	fmt.Println("Name:", pValue.FieldByName("Name").String()) // Name: Alice
	fmt.Println("Age:", pValue.FieldByName("Age").Int())      // Age: 18
}
