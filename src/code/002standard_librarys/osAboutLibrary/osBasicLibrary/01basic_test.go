package osBasicLibrary

import (
	"fmt"
	"os"
	"testing"
)

/**
os操作系统库 - 基本操作
- 1、模式
const (
	// The single letters are the abbreviations
	// used by the String method's formatting.
	ModeDir        FileMode = 1 << (32 - 1 - iota) // d: is a directory
	ModeAppend                                     // a: append-only
	ModeExclusive                                  // l: exclusive use
	ModeTemporary                                  // T: temporary file; Plan 9 only
	ModeSymlink                                    // L: symbolic link
	ModeDevice                                     // D: device file
	ModeNamedPipe                                  // p: named pipe (FIFO)
	ModeSocket                                     // S: Unix domain socket
	ModeSetuid                                     // u: setuid
	ModeSetgid                                     // g: setgid
	ModeCharDevice                                 // c: Unix character device, when ModeDevice is set
	ModeSticky                                     // t: sticky
	ModeIrregular                                  // ?: non-regular file; nothing else is known about this file

	// Mask for the type bits. For regular files, none will be set.
	ModeType = ModeDir | ModeSymlink | ModeNamedPipe | ModeSocket | ModeDevice | ModeCharDevice | ModeIrregular
	ModePerm FileMode = 0777 // Unix permission bits
)
*/

func TestCreate(t *testing.T) {
	/** 一、创建文件 os.Create()
	0、其实os.Create() == os.OpenFile(name, os.O_RDWR|os.O_CREATE, 0666)
	2、func Create(name string) (file *File, err error) {
		@param name: 目录名称
		return OpenFile(name, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0666)
	}
	3、Create采用0666模式(任何人都可读写，不可执行)，创建的文件有O_RDWR权限
	4、如果出错，错误底层类型位*PathError
	5、创建文件时，如果文件存在则会清空原文件内容
	*/
	f1, err1 := os.Create("test.txt")
	if err1 != nil {
		panic(err1)
	}
	defer f1.Close()
}

func TestMkdir(t *testing.T) {
	/** 二、创建单个目录 os.Mkdir()
	1、func Mkdir(name string, perm FileMode) error {
		@param name: 目录名称
		@param perm: 目录权限
		...
	}
	*/
	err2 := os.Mkdir("ms", os.ModePerm)
	if err2 != nil {
		fmt.Println("error2:", err2)
	}
}

func TestMkdirAll(t *testing.T) {
	/** 三、创建多级目录 os.MkdirAll()
	1、func MkdirAll(name string, perm FileMode) error {
		@param name: 目录名称
		@param perm: 目录权限
		...
	}
	*/
	err3 := os.MkdirAll("ms/a/b", os.ModePerm)
	if err3 != nil {
		fmt.Println("error3:", err3)
	}
}

func TestRemove(t *testing.T) {
	/** 四、删除一个空的目录或文件 os.Remove()
	1、func Remove(name string) error {}
	2、删除的目录下必须为空
	*/
	// err4 := os.Remove("ms/a/b")
	// if err4 != nil {
	// 	fmt.Println("error4:", err4)
	// }
	fmt.Println("")
}

func TestRemoveAll(t *testing.T) {
	/** 五、删除一整个目录或文件 os.RemoveAll()
	1、func RemoveAll(name string) error {}
	2、可删除整个目录
	*/
	err5 := os.RemoveAll("ms")
	if err5 != nil {
		fmt.Println("error5:", err5)
	}
}

func TestGetWd(t *testing.T) {
	/** 六、获取工作目录 os.GetWd()
	1、func GetWd() (dir string, err error) {}
	*/
	dir, err6 := os.Getwd()
	if err6 != nil {
		fmt.Println("error6:", err6)
	}
	fmt.Println("dir:", dir)
}

func TestChDir(t *testing.T) {
	/** 七、修改工作目录 os.ChDir()
	1、func (f *File) ChDir() err {}
	*/
	err := os.Chdir("ms")
	if err != nil {
		fmt.Println("error:", err)
	}
	fmt.Println(os.Getwd())
}

func TestTempDir(t *testing.T) {
	/** 八、获取临时目录
	1、func TempDir string {
		return tempDir()
	}
	*/
	s := os.TempDir()
	fmt.Println("s:", s) // s: /var/folders/zn/xxx/T/
}

func TestRename(t *testing.T) {
	/** 九、重命名文件 os.Rename()
	1、func Rename(oldPath, newPath string) error {
		return rename(oldPath, newPath)
	}
	*/
	// 修改文件名
	err1 := os.Rename("test.txt", "test2.txt")
	if err1 != nil {
		fmt.Println("error:", err1)
	}
	// 修改目录名
	err2 := os.Rename("ms", "ms1")
	if err2 != nil {
		fmt.Println("error:", err2)
	}
}

func TestChmod(t *testing.T) {
	/** 十、修改文件权限 os.Chmod()
	1、func Chmod(name string, mode FileMode) error {
		return chmod(name, mode)
	}
	*/
	err := os.Chmod("test.txt", 0111)
	if err != nil {
		fmt.Println("error:", err)
	}
}

func TestChown(t *testing.T) {
	/** 十、修改文件所有者 os.Chown()
	1、func Chown(name string, uid, gid int) error {
		...
	}
	2、cat /etc/passwd 查看uid和pid
	*/
	err := os.Chown("test.txt", 10, 10)
	if err != nil {
		fmt.Println("error:", err)
	}
}
