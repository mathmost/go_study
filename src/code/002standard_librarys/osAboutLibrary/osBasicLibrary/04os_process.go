package osBasicLibrary

import (
	"fmt"
	"os"
)

/**
进程相关
*/

func HHTestProcessExit() {
	/**
	1、让当前程序退出，0表示成功 非0表示出错，程序会立即终止，defer不会执行
	2、func Exit(code int)
	*/
	fmt.Println("")
}

func HHTestProcessGetuid() {
	/**
	1、获取调用者的用户id
	2、func Getuid() int
	*/
}

func HHTestProcessGeteuid() {
	/**
	1、获取调用者的有效用户id
	2、func Geteuid() int
	*/
}

func HHTestProcessGetgid() {
	/**
	1、获取调用者的组id
	2、func Getgid() int
	*/

}

func HHTestProcessGetegid() {
	/**
	1、获取调用者的有效组id
	2、func Getegid() int
	*/
}

func HHTestProcessGetGroups() {
	/**
	1、获取调用者所在组的所有组id
	2、func GetGroups() ([]int, error)
	*/
}

func HHTestProcessGetpid() {
	/**
	1、获取调用者所在进程的id
	2、func Getpid() int
	*/
	fmt.Println("获取当前正在运行的pid:", os.Getpid())
}

func HHTestProcessGetppid() {
	/**
	1、获取调用者所在进程的父进程的id
	2、func Getppid() int
	*/
	fmt.Println("获取当前正在运行的pid的父进程id:", os.Getppid())
}
