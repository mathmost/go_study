package osBasicLibrary

import (
	"fmt"
	"os"
)

func HHTestWrite() {
	/**
	1、Write向文件写入len(b)字节数据，返回写入的字节数和可能出现的错误；
	2、如果返回值n != len(b)，本方法会返回一个非nil的错误
	3、func (f *File) Write(b []byte) (n int, err error) {}
	*/
	file, err := os.OpenFile("test1.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)
	if err != nil {
		fmt.Println("write error:", err)
	}
	defer file.Close()

	n, _ := file.Write([]byte("hello python!\n"))
	fmt.Println("write n:", n)
}

func HHTestWriteString() {
	/**
	1、WriteString类似Write，但接收一个字符串参数
	2、func (f *File) WriteString(s string) (ret int, err error) {}
	*/
	file, err := os.OpenFile("test1.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)
	if err != nil {
		fmt.Println("write string error:", err)
	}
	defer file.Close()

	n, _ := file.WriteString("hello java!\n")
	fmt.Println("writeString n:", n)
}

func HHTestWriteAt() {
	/**
	1、在指定的位置向文件写入len(b)字节数据，返回写入的字节数和可能出现的错误；
	2、如果返回值n != len(b)，本方法会返回一个非nil的错误
	2、func (f *File) WriteAt(b []byte， off int64) (n int, err error) {}
	*/
	file, err := os.OpenFile("test1.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0755)
	if err != nil {
		fmt.Println("write error:", err)
	}
	defer file.Close()

	n, _ := file.WriteAt([]byte("insert hello javascript!\n"), 5)
	fmt.Println("write n:", n)
}
