package osBasicLibrary

import (
	"fmt"
	"os"
)

// 环境相关

func HHTestEnvHostname() {
	// 获取主机名
	// func Hostname() (name string, err error) {}
	hostname, _ := os.Hostname()
	fmt.Println("Hostname:", hostname)
}

func HHTestEnviron() {
	// 获取所有
	// func Environ() string {}
	fmt.Println("Environ:", os.Environ())
}

func HHTestEnvGetenv() {
	// 获取某个环境变量
	// func Getenv(key string) string {}
	fmt.Println("Getenv:", os.Getenv("GOPATH"))
}

func HHTestEnvSetenv() {
	// 设置环境变量
	// func Setenv(key, value string) error {}
	fmt.Println("Setenv:", os.Setenv("env1", "val1"))
}

func HHTestEnvLookupEnv(envname string) {
	// 查找某个环境变量是否存在
	// func LookupEnv(key string) (string, bool) {}
	s, ok := os.LookupEnv(envname)
	fmt.Println("s:", s)
	fmt.Println("ok(该环境变量是否存在):", ok)
}

func HHTestEnvClearenv() {
	// 删除当前程序已有的环境变量
	// func Clearenv()
	os.Clearenv()
}
