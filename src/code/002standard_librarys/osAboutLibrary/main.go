package main

import (
	// osBasicLibrary "hello/src/code/002standard_librarys/osAboutLibrary/osBasicLibrary"
	osFileLibrary "hello/src/code/002standard_librarys/osAboutLibrary/osFileLibrary"
)

func main() {
	// 1、osBasicLibrary
	// osBasicLibrary.HHTestFileStat()
	// osBasicLibrary.HHTestFileRead()
	// osBasicLibrary.HHTestFileReadAt()
	// osBasicLibrary.HHTestFileReadDir()
	// osBasicLibrary.HHTestFileSeek()

	// 2、osFileLibrary
	osFileLibrary.CopyDirToNewDir()
	osFileLibrary.ReadWriteFiles()
	osFileLibrary.SideReadWriteFiles()
	osFileLibrary.LineSingleReadFiles()
	osFileLibrary.LineBufioReadFiles()
}
