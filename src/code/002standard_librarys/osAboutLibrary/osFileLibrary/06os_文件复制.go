package osFileLibrary

import (
	"io"
	"log"
	"os"
	"path"
)

// 复制目录下的所有文件 到 一个新的目录下
func CopyDirToNewDir() {
	list := getFiles(sourceDir)
	for _, f := range list {
		_, name := path.Split(f)
		destFile := destDir + "copy/" + name
		// 复制文件
		CopyFile(f, destFile)
	}
}

func CopyFile(srcName, destName string) (int64, error) {
	src, err := os.Open(srcName)
	if err != nil {
		log.Fatal(err)
	}
	defer src.Close()

	dst, err := os.OpenFile(destName, os.O_CREATE|os.O_WRONLY, 0655)
	if err != nil {
		log.Fatal(err)
	}
	defer dst.Close()

	return io.Copy(dst, src)
}
