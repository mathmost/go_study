package osFileLibrary

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

// 小文件: 一次性读取
func LineSingleReadFiles() {
	var readmeDir string = sourceDir + "/readme.md"
	fileHandler, err := os.OpenFile(readmeDir, os.O_RDONLY, 0655)
	if err != nil {
		log.Fatal(err)
	}
	defer fileHandler.Close()

	// 按行一次性读取
	bytes, err := io.ReadAll(fileHandler)
	if err != nil {
		log.Fatal(err)
	}

	// 分割数据
	list := strings.Split(string(bytes), "\n")
	for _, l := range list {
		fmt.Println("l:", l)
	}
}

// bufio读取: 数据缓冲功能能减少一定程度的开销
func LineBufioReadFiles() {
	var readmeDir string = sourceDir + "/readme.md"
	fileHandler, err := os.OpenFile(readmeDir, os.O_RDONLY, 0655)
	if err != nil {
		log.Fatal(err)
	}
	defer fileHandler.Close()

	reader := bufio.NewReader(fileHandler)
	for {
		line, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		fmt.Println("line:", line)
	}
}
