package osFileLibrary

import (
	"io"
	"log"
	"os"
	"path"
)

func SideReadWriteFiles() {
	list := getFiles(sourceDir)
	for _, f := range list {
		_, name := path.Split(f)
		destName := "/" + destDir + "side/" + name

		// 分片读取写入文件
		SideWriteFile(f, destName, 1024)
	}
}

func SideWriteFile(srcName, destName string, size int) {
	src, err := os.Open(srcName)
	if err != nil {
		log.Fatal(err)
	}
	defer src.Close()

	dst, err := os.OpenFile(destName, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0655)
	if err != nil {
		log.Fatal(err)
	}
	defer dst.Close()

	buffer := make([]byte, size)
	for {
		n, err := src.Read(buffer)
		if err != nil && err != io.EOF {
			log.Fatal(err)
		}
		if n == 0 {
			break
		}
		dst.WriteString(string(buffer))
	}
}
