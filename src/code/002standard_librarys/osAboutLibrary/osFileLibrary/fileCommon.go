package osFileLibrary

import (
	"log"
	"os"
	"strings"
)

// 定义常量
var abspathWdDir, _ = os.Getwd()
var sourceDir = abspathWdDir + "/osFileLibrary/source_file/"
var destDir = abspathWdDir + "/osFileLibrary/dest_file/"

// 获取目录下的所有文件
func getFiles(dir string) []string {
	fs, err := os.ReadDir(dir)
	if err != nil {
		log.Fatal(err)
	}

	list := make([]string, 0)
	for _, f := range fs {
		if f.IsDir() {
			continue
		}
		fullName := "/" + strings.Trim(dir, "/") + "/" + f.Name()
		list = append(list, fullName)
	}
	return list
}
