//@Author: cyl
//@File: main.go
//@Time: 2023/06/23 23:31:42
package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	// ReadFile(): 读取文件中的所有数据, 返回byte, 底层调用的就是ReadAll()方法
	data1, err1 := ioutil.ReadFile("./test2.txt")
	fmt.Println("read file err:", err1)
	fmt.Println("read file data:", string(data1))

	// WriteFile(): 写入数据, 当文件不存在时会新建, 如果文件存在则清空文件内容再写入
	s1 := "春暖花开"
	err2 := ioutil.WriteFile("./test3.txt", []byte(s1), os.ModePerm)
	fmt.Println("write file err:", err2)

	// ReadAll(): 读取Reader类型的数据
	s2 := "床前明月光, 疑是地上霜"
	r1 := strings.NewReader(s2)
	data2, _ := ioutil.ReadAll(r1)
	fmt.Println("read all data:", string(data2)) // "床前明月光, 疑是地上霜"

	// ReadDir(): 读取目录内容, 返回[]fs.FileInfo类型(只返回·第一层·目录里面的文件和子文件)
	dirName := "."
	fileInfos, _ := ioutil.ReadDir(dirName)
	fmt.Println("该目录存在文件和子文件数量:", len(fileInfos))
	for i := 0; i < len(fileInfos); i++ {
		fmt.Printf("第%d个文件: 名称为%s, 是否为目录%t\n", i+1, fileInfos[i].Name(), fileInfos[i].IsDir())
	}

	// tempDir(), TempFile(): 创建一个临时目录 或 临时文件, 可读可写
	temp_dir, _ := ioutil.TempDir(".", "test")
	fmt.Println("temp dir:", temp_dir)
	defer os.Remove(temp_dir)
	temp_file, _ := ioutil.TempFile(temp_dir, "test4")
	fmt.Println("temp file:", temp_file)
	defer os.Remove(temp_file.Name())
}
