//@Author: cyl
//@File: main.go
//@Time: 2022/06/09 09:32:16
package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

func printBody(r *http.Request) {
	request, err := http.DefaultClient.Do(r)
	if err != nil {
		panic(err)
	}
	defer request.Body.Close()

	content, err := ioutil.ReadAll(request.Body)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s:\n", content)
}

func requestWithParams() {
	request, err := http.NewRequest(http.MethodGet, "http://httpbin.org/get", nil)
	if err != nil {
		panic(err)
	}
	params := make(url.Values)
	params.Add("name", "geigei")
	params.Add("age", "18")
	request.URL.RawQuery = params.Encode()
	printBody(request)
}

func requestWithHeader() {
	request, err := http.NewRequest(http.MethodGet, "http://httpbin.org/get", nil)
	if err != nil {
		panic(err)
	}
	request.Header.Add("user-agent", "chrome")
	printBody(request)
}

func main() {
	// 1. 设置请求参数
	requestWithParams()
	// 2. 设置请求头
	requestWithHeader()
}
