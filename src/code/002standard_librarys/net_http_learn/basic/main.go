//@Author: cyl
//@File: main.go
//@Time: 2022/06/09 09:14:59
package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func get() {
	r, err := http.Get("http://httpbin.org/get")
	if err != nil {
		panic(err)
	}
	defer r.Body.Close()
	content, _ := ioutil.ReadAll(r.Body)
	fmt.Printf("get, %s:\n", content)
}

func post() {
	r, err := http.Post("http://httpbin.org/post", "", nil)
	if err != nil {
		panic(err)
	}
	defer r.Body.Close()
	content, _ := ioutil.ReadAll(r.Body)
	fmt.Printf("post, %s:\n", content)
}

func put() {
	request, err := http.NewRequest(http.MethodPut, "http://httpbin.org/put", nil)
	if err != nil {
		panic(err)
	}
	r, err_do := http.DefaultClient.Do(request)
	if err_do != nil {
		panic(err_do)
	}
	defer r.Body.Close()
	content, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	fmt.Printf("put, %s:\n", content)
}

func delete() {
	request, err := http.NewRequest(http.MethodDelete, "http://httpbin.org/delete", nil)
	if err != nil {
		panic(err)
	}
	r, err_do := http.DefaultClient.Do(request)
	if err_do != nil {
		panic(err_do)
	}
	defer r.Body.Close()
	content, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	fmt.Printf("delete, %s:\n", content)
}

func main() {
	get()
	post()
	put()
	delete()
}
