//@Author: cyl
//@File: main.go
//@Time: 2022/06/09 10:46:07
package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
)

func postForm() {
	form_data := make(url.Values)
	form_data.Add("name", "mary")
	form_data.Add("age", "18")
	payLoad := form_data.Encode()
	r, err := http.Post(
		"http://httpbin.org/post",
		"application/x-www-form-urlencoded",
		strings.NewReader(payLoad))
	if err != nil {
		panic(err)
	}
	defer r.Body.Close()

	content, _ := ioutil.ReadAll(r.Body)
	fmt.Printf("%s", content)
}

func postJson() {
	u := struct {
		Name string `json:"name"`
		Age  int    `json:"age"`
	}{
		Name: "john",
		Age:  20,
	}
	payLoad, _ := json.Marshal(u)
	r, err := http.Post(
		"http://httpbin.org/post",
		"application/json",
		bytes.NewReader(payLoad))
	if err != nil {
		panic(err)
	}
	defer r.Body.Close()

	content, _ := ioutil.ReadAll(r.Body)
	fmt.Printf("%s", content)
}

func main() {
	postForm()
	postJson()
}
