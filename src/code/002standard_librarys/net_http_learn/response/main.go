//@Author: cyl
//@File: main.go
//@Time: 2022/06/09 09:59:00
package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"net/http"

	"golang.org/x/net/html/charset"
)

func content(r *http.Response) {
	content, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s:\n", content)
}

func status(r *http.Response) {
	fmt.Println("status code:", r.StatusCode)
}

func header(r *http.Response) {
	fmt.Println(r.Header.Get("content-type")) // 使用get获取可自动忽略大下写
	// fmt.Println(r.Header["Content-Type"])  // 不推荐使用
}

func encoding(r *http.Response) {
	/**获取编码信息，通常用于获取网页的编码*/
	// 使用go get -u golang.org/x/net/html
	bufReader := bufio.NewReader(r.Body)
	bytes, _ := bufReader.Peek(1024)
	e, _, _ := charset.DetermineEncoding(bytes, r.Header.Get("content-type"))
	fmt.Println("e:", e)
}

func main() {
	r, err := http.Get("http://httpbin.org/get")
	if err != nil {
		panic(err)
	}
	defer r.Body.Close()

	content(r)
	status(r)
	header(r)
	encoding(r)
}
