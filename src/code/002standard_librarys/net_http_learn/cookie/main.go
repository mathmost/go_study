//@Author: cyl
//@File: main.go
//@Time: 2022/06/09 11:20:46
package main

import (
	"io"
	"net/http"
	"net/http/cookiejar"
	"os"
	"time"
)

// cookie持久化
func jarCookie() {
	jar, _ := cookiejar.New(nil)
	client := &http.Client{
		Jar:     jar,
		Timeout: 10 * time.Second,
	}
	r, err := client.Get("http://httpbin.org/cookies/set?username=mary&password=111111")
	if err != nil {
		panic(err)
	}
	defer r.Body.Close()
	_, _ = io.Copy(os.Stdout, r.Body)
}

func main() {
	jarCookie()
}
