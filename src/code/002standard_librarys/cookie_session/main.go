package main

import (
	"fmt"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/redis"
	"github.com/gin-gonic/gin"
)

// cookie session

func main() {
	r := gin.Default()
	r.GET("/cookie", func(c *gin.Context) {
		// 设置cookie
		// name: 名称, 相当于key(不允许带特殊符号, 否则无法获取到Cookie的值)
		// value: 设置的cookie内容
		// maxAge: cookie的最大生命周期 0: 表示没有指定该属性; <0 表示删除; >0 指定生命周期, 单位s
		// path: 路径, 通常给""
		// domain: 域名 IP地址
		// secure: 设置是否安全保护
		// httpOnly: 是否只针对http协议
		c.SetCookie("TestCookie", "123456", 60, "", "", false, true)
		// 获取cookie
		str, _ := c.Cookie("TestCookie")
		fmt.Println("获取到的cookie:", str)
		c.JSON(http.StatusOK, gin.H{
			"message": "set cookie success",
		})
	})

	// 初始化容器
	// 10: 容器大小
	// tcp: 协议
	// []byte("secret"): 表示加密用的密钥
	store, _ := redis.NewStore(10, "tcp", "127.0.0.1:6379", "", []byte("secret"))
	// 使用容器
	r.Use(sessions.Sessions("MySession", store))

	r.GET("/session", func(c *gin.Context) {
		// gin框架默认不支持session，需要使用中间件 --- ginMiddleWare
		// 设置session, 使用官方案例: https://github.com/gin-contrib/sessions
		session := sessions.Default(c)
		var count int
		v := session.Get("count")
		if v == nil {
			count = 0
		} else {
			count = v.(int)
			count++
		}
		session.Set("count", count)
		session.Save()
		c.JSON(http.StatusOK, gin.H{
			"count": count,
		})
	})

	r.Run(":9000")
}
