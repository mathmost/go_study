//@Author: cyl
//@File: request.go
//@Time: 2022/06/09 00:36:38
package main

import (
	_ "fmt"
	"io"
	"io/ioutil"
	"net/http"
)

func RequestHandle(method, prefix string, cookie *http.Cookie, body io.Reader) (string, error) {
	client := http.Client{}
	var resp *http.Request
	if body == nil {
		resp, _ = http.NewRequest(method, BaseUrl+prefix, nil)
	} else {
		resp, _ = http.NewRequest(method, BaseUrl+prefix, body)
	}
	// 添加请求头
	resp.Header.Add("Content-type", "application/x-www-form-urlencoded")
	// 添加cookie
	resp.AddCookie(cookie)
	// 发起请求
	resp_do, err_do := client.Do(resp)
	if err_do != nil {
		panic("err")
	}
	defer resp_do.Body.Close()
	// 获取请求响应
	result, err := ioutil.ReadAll(resp_do.Body)
	if err != nil {
		panic("err")
	}
	return string(result), err
}
