//@Author: cyl
//@File: main.go
//@Time: 2022/06/07 23:25:23
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	"github.com/tidwall/gjson"
)

type GoodsSupplyRequestParam struct {
	GoodsCondition int `json:"goodsCondition"`
	GoodsStatus    int `json:"goodsStatus"` // 货源状态
	PageIndex      int `json:"pageIndex"`
	CurrentPage    int `json:"currentPage"`
	PageSize       int `json:"pageSize"`
}

func main() {
	// 1. 登陆
	params := url.Values{}
	params.Set("mobile", "17802156775")
	params.Set("password", "123456")
	resp2, err2 := http.PostForm(BaseUrl+"/user/login?", params)
	if err2 != nil {
		return
	}
	defer resp2.Body.Close()
	body, err := ioutil.ReadAll(resp2.Body)
	if err != nil {
		return
	}
	// 获取request、response、status_code、*http.Cookie
	fmt.Println(resp2.Request.Method)
	fmt.Println(resp2.Request.URL)
	status_code, cookie := resp2.StatusCode, resp2.Cookies()[0]
	// 获取response
	var strBody = string(body)
	if_success := gjson.Get(strBody, "ifSuccess")
	fmt.Println("if_success:", if_success)
	if status_code == 200 {
		// 2. 报价中的货源
		body3, _ := RequestHandle(http.MethodPost, "/manage/myGoods/1", cookie, nil)
		fmt.Println("报价中的货源数:", gjson.Get(string(body3), "myGoodsCount"))

		// 3. 我的货源
		jsonBody, _ := json.Marshal(&GoodsSupplyRequestParam{
			GoodsCondition: 4,
			GoodsStatus:    2131000,
			PageIndex:      0,
			PageSize:       5,
			CurrentPage:    0,
		})
		resp4Body := strings.NewReader(string(jsonBody))
		body4, _ := RequestHandle(http.MethodPost, "/manage/goodssupply/list", cookie, resp4Body)
		fmt.Println("我的货源:", string(body4))
	}
}
