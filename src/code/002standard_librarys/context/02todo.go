//@Author: cyl
//@File: todo.go
//@Time: 2023/06/30 13:13:53
package main

import (
	"context"
	"fmt"
	"time"
)

func f1() {
	ctx, cancel := context.WithTimeout(context.TODO(), 3*time.Second)
	defer cancel()

	// 验证3秒后，管道是否关闭并执行Deadlione()
	select {
	case <-ctx.Done():
		err := ctx.Err()
		fmt.Println("err:", err) // err: context deadline exceeded
	}
}
