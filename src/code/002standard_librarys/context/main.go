//@Author: cyl
//@File: main.go
//@Time: 2023/06/30 13:19:27
package main

/**
context上下文
1、在golang中的定义
	type Context struct {
		Deadline() (deadline time.Time, ok bool) // 超时函数
		Done() <-chan struct{}                   // 返回一个管道，而且还是一个只读管道
		Err() error							     // error
		Value(key any) any						 // value
	}
2、golang源码中的实现
type emptyCtx struct{}

func (*emptyCtx) Deadline() (deadline time.Time, ok bool) {return}
func (*emptyCtx) Done()<-chan struct{} {return nil}
func (*emptyCtx) Err() error {return nil}
func (*emptyCtx) Value(key any) any {return nil}

3、作用
	a. 一方面是关于key value的使用, 可以在一个项目中不停的添加key value, 在后续步骤中读取, 好处则是可以把很多参数封装在context中, 避免太冗余的参数传递
	b. 跟管道相关的，通过超时机制或通过显示的cancal来关闭管道, 而管道一旦关闭则相对应的读操作就会解除阻塞, 这种机制通常用于超时的控制
*/

func main() {
	// 一、关于context中 value 使用
	// grandpa := context.TODO()
	// father := step1(grandpa)
	// grandson := step2(father) // 每一次更新step，也就是设置一个child后，都会将上一个step中的值全部继承至当前step中
	// step3(grandson)           // name 孙悟空, age 500

	// 二、关于context中 TODO 使用
	// f1()

	// 三、关于context中 TODO timeout 使用
	// f2()

	// 四、关于context中 cancel 使用
	f3()
}
