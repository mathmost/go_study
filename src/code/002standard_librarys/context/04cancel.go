package main

import (
	"context"
	"fmt"
	"time"
)

func f3() {
	cancelCtx, cancel := context.WithCancel(context.TODO())
	t0 := time.Now()
	// 起一个协程调用cancel函数
	// 当sleep100ms后，调用cancel()，则管道被关闭
	go func() {
		time.Sleep(100 * time.Millisecond)
		cancel()
	}()
	select {
	case <-cancelCtx.Done():
		err := cancelCtx.Err()
		t3 := time.Now()
		fmt.Println("t3到t0的间隔时间:", t3.Sub(t0).Milliseconds()) // 101
		fmt.Println("err:", err)                              // err: context deadline exceeded
	}
}
