package main

import (
	"net/http"

	"github.com/natefinch/lumberjack"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// zap日志库

// 文档: https://www.liwenzhou.com/posts/Go/zap/#autoid-1-4-2

/*
	1. go get -u go.uber.org/zap
	2. zap提供了两种类型的日志记录器—Sugared Logger和Logger
	3. 在性能很好但不是很关键的上下文中，使用SugaredLogger;
	   它比其他结构化日志记录包快4-10倍，并且支持结构化和printf风格的日志记录
	4. 在每一微秒和每一次内存分配都很重要的上下文中，使用Logger;
	   它甚至比SugaredLogger更快，内存分配次数也更少，但它只支持强类型的结构化日志记录
*/

var sugarLogger *zap.SugaredLogger

func main() {
	InitLogger()
	defer sugarLogger.Sync()

	simpleHttpGet("www.sogo.com")
	simpleHttpGet("http://www.sogo.com")
	sugarLogger.Infof("今早起来打了三个喷嚏, 等下到公司把药喝了!")
}

// 初始化logger
func InitLogger() {
	writeSyncer := getLogWriter()
	encoder := getEncoder()
	core := zapcore.NewCore(encoder, writeSyncer, zapcore.DebugLevel)

	logger := zap.New(core, zap.AddCaller())
	sugarLogger = logger.Sugar()
}

// 将JSON Encoder更改为普通的Log Encoder
func getEncoder() zapcore.Encoder {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder   // 修改时间编码器
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder // 在日志文件中使用大写字母记录日志级别
	return zapcore.NewConsoleEncoder(encoderConfig)
}

// Lumberjack进行日志切割归档
func getLogWriter() zapcore.WriteSyncer {
	lumberJackLogger := &lumberjack.Logger{
		Filename:   "./test.log", // 日志文件的位置
		MaxSize:    1,            // 在进行切割之前，日志文件的最大大小（以MB为单位）
		MaxBackups: 5,            // 保留旧文件的最大个数
		MaxAge:     30,           // 保留旧文件的最大天数
		Compress:   false,        // 是否压缩/归档旧文件
	}
	return zapcore.AddSync(lumberJackLogger)
}

// 模仿请求
func simpleHttpGet(url string) {
	sugarLogger.Debugf("Trying to hit GET request for %s", url)
	resp, err := http.Get(url)
	if err != nil {
		sugarLogger.Errorf("Error fetching URL %s : Error = %s", url, err)
	} else {
		sugarLogger.Infof("Success! statusCode = %s for URL %s", resp.Status, url)
		resp.Body.Close()
	}
}
