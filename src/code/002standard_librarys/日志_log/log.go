package main

import (
	"fmt"
	"log"
	"os"
)

func main() {
	log.Println("这是一条普通的日志") // 2020/07/18 21:30:39 这是一条普通的日志

	var v = "format"
	log.Printf("这是一条%s的日志", v) // 2020/07/18 21:34:34 这是一条format的日志

	// flag选项: 设置日志输出信息
	log.SetFlags(log.Llongfile | log.Lmicroseconds | log.Ldate)
	log.Println("这是一条设置输出信息的日志") // 2020/07/18 21:36:11 .../log.go:15: 这是一条设置输出信息的日志

	// 配置日志前缀
	log.SetFlags(log.Llongfile | log.Lmicroseconds | log.Ldate)
	log.Println("这是一条很普通的日志") // 2020/07/18 21:38:03 .../log.go:19: 这是一条很普通的日志
	log.SetPrefix("[小王子]")
	log.Println("这是一条很普通的日志") // [小王子]2020/07/18 21:38:03 .../log.go:21: 这是一条很普通的日志

	// 配置日志输出位置
	logFile, err := os.OpenFile("./current.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		fmt.Println("open log file failed, err:", err)
		return
	}
	log.SetOutput(logFile)
	log.SetFlags(log.Llongfile | log.Lmicroseconds | log.Ldate)
	log.Println("这是一条很普通的日志")
}
