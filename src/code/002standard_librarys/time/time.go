package main

import (
	"fmt"
	"time"
)

func main() {
	// 获取当前时间
	now := time.Now()
	fmt.Println(now)
	fmt.Println("年:", now.Year())      // 200
	fmt.Println("月:", now.Month())     // June
	fmt.Println("月", int(now.Month())) // 06
	fmt.Println("日:", now.Day())       // 04
	fmt.Println("时:", now.Hour())      // 23
	fmt.Println("分:", now.Minute())    // 50
	fmt.Println("秒:", now.Second())    //28

	// 时间戳
	fmt.Println(now.Unix())     // 1654357382
	fmt.Println(now.UnixNano()) // 纳秒时间戳 1654357382954436000

	// 解析字符串格式的时间
	// _, err := time.LoadLocation("Asia/Shanghai")
	// if err != nil {
	// 	fmt.Println(err)
	// 	return
	// }
	// var nowTime = time.Unix(time.Now().Unix(), 0)
	// fmt.Println(nowTime)

	// 时区
	loc, _ := time.LoadLocation("Asia/Shanghai")
	parse, _ := time.ParseInLocation("2006/01/02 15:04:05", "2023/06/02 18:00:00", loc)
	subLocationTime := parse.Sub(now)
	fmt.Println("时区时间:", subLocationTime)

	// 格式化时间
	fmt.Println(now.Format("2006-01-02 15:04:05")) // 2022-01-13 22:25:16
	fmt.Println(now.Format("2006-01-02"))          // 2022-01-13
	fmt.Println(now.Format("15:04:05"))            // 22:25:16
	fmt.Println(now.Format("2006"))                // 2022
	fmt.Println(now.Format("01"))                  // 01
	fmt.Println(now.Format("02"))                  // 13

	// 时间常量
	// time.Sleep(time.Millisecond): Sleep阻塞当前go程至少d代表的时间段。d<=0时，Sleep会立刻返回
	sleep_count := 0
	for {
		sleep_count++
		fmt.Println(sleep_count)
		// 每休眠1秒 打印sleep_count的值，当值等于5时终止
		time.Sleep(time.Second)
		if sleep_count == 5 {
			break
		}
	}

	fiveMinute := 5 * time.Minute
	// Add
	newAddTime := now.Add(time.Hour)
	fmt.Println("当前时间+1H:", newAddTime)
	newAddFiveMinuteTime := now.Add(fiveMinute)
	fmt.Println("当前时间+5M:", newAddFiveMinuteTime)

	// Sub
	subTime := newAddFiveMinuteTime.Sub(now)
	fmt.Println("减去当前时间:", subTime) // 5m0s

	// After: func (t Time) After(u time) bool {}
	// Before: func (t Time) Before(u time) bool {}
	// Equal: func (t Time) Equal(u time) bool {}
	currentTime := time.Now()
	fmt.Println("时间是否相等Equal:", now.Equal(currentTime))
	fmt.Println("时间是否相等Equal:", now == currentTime)
	fmt.Println("时间是否Before:", now.Before(currentTime))
	fmt.Println("时间是否After:", now.After(currentTime))

	// 定时器: 本质上是一个通道(channel)
	// ticker := time.Tick(time.Second) // 定义一个1s间隔的定时器
	// for i := range ticker {
	// 	fmt.Println("i:", i) // 在定时器中每秒会一直执行任务
	// }

	// AfterFunc()
	time.AfterFunc(5*time.Second, func() {
		fmt.Println("5s之后才户执行这个方法")
	})

	// 练习
	// 1 获取当前时间，格式化输出为`2017/06/19 20:30:05`格式
	nowTime := time.Now().Unix()
	fmt.Println(time.Unix(nowTime, 0).Format("2006/01/02 15:04:05"))
	// 2 编写程序统计一段代码的执行耗时时间，单位精确到微秒
	start := time.Now().UnixNano()
	var sum = 0
	for i := 1; i < 1000; i++ {
		sum += i
	}
	fmt.Println(sum)
	end := time.Now().UnixNano()
	fmt.Println("耗时: ", end-start)
}
