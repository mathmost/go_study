//@Author: cyl
//@File: 01rand比较.go
//@Time: 2023/07/02 17:49:17
package main

import (
	"bytes"
	"crypto/rand"
	"fmt"
)

/**
之前发现golang标准库中有两个rand软件包,开始非常想知道他们之间的差异;
math/rand软件包可以用于简单的游戏,但不能用于真正的随机性
	math/rand : 伪随机数生成器
	crypto/rand: 加密安全的随机数生成器
*/

func main() {
	// 1、从 rand.Reader 中读取10个密码安全的伪随机数，并将它们写入字节
	c := 10
	b := make([]byte, 10)
	_, err := rand.Read(b)
	if err != nil {
		fmt.Println("error:", err)
		return
	}
	// 切片现在应该包含随机字节而不是仅包含零
	fmt.Println(bytes.Equal(b, make([]byte, c))) // false
}
