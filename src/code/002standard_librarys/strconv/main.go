package main

import (
	"fmt"
	"strconv"
)

/** 类型转换 */

func main() {
	// 1. Atoi() str -> int
	s1 := "100"
	il, err := strconv.Atoi(s1)
	if err != nil {
		fmt.Println("cant convert to int")
	} else {
		fmt.Printf("type: %T\n", il) // type: int
	}

	// 2. Itoa() int -> str
	i2 := 18
	s2 := strconv.Itoa(i2)
	fmt.Printf("type: %T\n", s2) // type: string

	// 3. Parse类函数用于转换字符串为给定类型的值
	b, _ := strconv.ParseBool("true")
	f, _ := strconv.ParseFloat("3.1415", 64)
	i, _ := strconv.ParseInt("-2", 10, 64)
	u, _ := strconv.ParseUint("2", 10, 64)
	fmt.Printf("type: %T, %v\n", b, b) // type: bool, true
	fmt.Printf("type: %T, %v\n", f, f) // type: float64, 3.1415
	fmt.Printf("type: %T, %v\n", i, i) // type: int64, -2
	fmt.Printf("type: %T, %v\n", u, u) // type: uint64, 2

	// 4. Format系列函数实现了将给定类型数据格式化为string类型数据的功能
	ss1 := strconv.FormatBool(true)
	ss2 := strconv.FormatFloat(3.1415, 'E', -1, 64)
	ss3 := strconv.FormatInt(-2, 16)
	ss4 := strconv.FormatUint(2, 16)
	fmt.Printf("type: %T, %v\n", ss1, ss1) // type: string, true
	fmt.Printf("type: %T, %v\n", ss2, ss2) // type: string, 3.1415E+00
	fmt.Printf("type: %T, %v\n", ss3, ss3) // type: string, -2
	fmt.Printf("type: %T, %v\n", ss4, ss4) // type: string,  2

	// 5. 其他
	// isPrint() 判断一个字符是否可以打印, 和unicode.IsPrint一样
	// CanBackquote() 返回字符串s是否可以不被修改的表示为一个单行的、没有空格和tab之外控制字符的反引号字符串
}
