//@Author: cyl
//@File: main.go
//@Time: 2023/07/01 16:37:33
package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"path"

	"github.com/sirupsen/logrus"
)

func main() {
	file, _ := os.OpenFile("yourLogPath.log", os.O_CREATE|os.O_APPEND|os.O_RDWR, 0655)
	// 日志输出到文件和控制台
	logrus.SetOutput(io.MultiWriter(file, os.Stdout))
	// 设置日志级别：如果设置了级别后，后续日志中比设置的级别还高级别的日志将不会输出
	logrus.SetLevel(logrus.DebugLevel)
	// 开启返回函数名和行号
	logrus.SetReportCaller(true)

	// 自定义日志格式: TextFormatter
	/**
	type TextFormatter struct{
		//颜色显示相关
		ForceColors bool
		EnvironmentOverrideColors bool
		DisableColors bool

		//日志中的键值对加引号相关
		ForceQuote bool
		DisableQuote bool
		QuoteEmptyFields bool

		//时间戳相关
		DisableTimestamp bool
		FullTimestamp bool
		TimestampFormat string
	}
	*/
	// logrus.SetFormatter(&logrus.TextFormatter{
	// 	ForceColors:     true,                  // 针对每个级别的日志附上不同的颜色
	// 	FullTimestamp:   true,                  // 是否输出时间
	// 	TimestampFormat: "2006-01-02 15:04:05", // 时间的格式
	// })

	// 自定义日志格式: JSONFormatter
	/**
	1、结构体
		type JSONFormatter struct {
			//时间戳相关
			TimestampFormat string
			DisableTimestamp bool

			DisableHTMLEscape bool

			// PrettyPrint will indent all json logs
			PrettyPrint bool
		}
	2、示例
		func main(){
			logrus.SetFormatter(&logrus.JSONFormatter{
				TimestampFormat:"2006-01-02 15:04:05",
				PrettyPrint: true,
			})
			logrus.WithField("name", "ball").WithField("say", "hi").Info("info log")
		}

	3、输出
		{
		"level": "info",
		"msg": "info log",
		"name": "ball",
		"say": "hi",
		"time": "2021-05-10 16:36:05"
		}
	*/

	// 自定义日志格式
	logrus.SetFormatter(&MyFormatter{
		Prefix:     "Gorm",
		TimeFormat: "2006-01-02 15:04:05",
	})

	// 获取logrus级别
	fmt.Println(logrus.GetLevel())

	logrus.Info("hi, info")
	logrus.Warning("hi, warning")
	logrus.Error("hi, error")
	logrus.Debugln("hi, debug")
}

const (
	ccBlack  = 0
	ccRed    = 1
	ccGreen  = 2
	ccYellow = 3
	ccBlue   = 4
	ccPurple = 5
	ccCyan   = 6
	ccGray   = 7
)

type MyFormatter struct {
	Prefix     string // 设置日志的前缀
	TimeFormat string // 设置日志的日期格式
}

func (m *MyFormatter) Format(entry *logrus.Entry) ([]byte, error) {
	// 根据日志级别设置不同的颜色
	var color int
	switch entry.Level {
	case logrus.DebugLevel:
		color = ccGreen
	case logrus.InfoLevel:
		color = ccBlue
	case logrus.WarnLevel:
		color = ccYellow
	case logrus.ErrorLevel:
		color = ccRed
	default:
		color = ccGreen
	}

	// 设置buffer缓冲期
	var b *bytes.Buffer
	if entry.Buffer != nil {
		b = entry.Buffer
	} else {
		b = &bytes.Buffer{}
	}
	// 设置日志时间格式
	timestamp := entry.Time.Format(m.TimeFormat)

	// 设置文件的行号
	// fileVal := fmt.Sprintf("%s:%d", entry.Caller.File, entry.Caller.Line)
	// fileVal := fmt.Sprintf("%s:%d", path.Base(entry.Caller.File), entry.Caller.Line) // 只取文件的相对路径

	// 设置日志的函数名
	fileVal := fmt.Sprintf("%s:%s:%d", entry.Caller.Function, path.Base(entry.Caller.File), entry.Caller.Line)

	// 设置格式
	// var newLog string
	// newLog = fmt.Sprintf("\033[3%dm[%s]\033[0m [%s] %s\n", color, entry.Level, timestamp, entry.Message)
	// b.WriteString(newLog)

	fmt.Fprintf(b, "[%s] \033[3%dm[%s]\033[0m [%s] [%s] %s\n", m.Prefix, color, entry.Level, timestamp, fileVal, entry.Message)
	return b.Bytes(), nil
}
