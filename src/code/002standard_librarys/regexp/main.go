//@Author: cyl
//@File: main.go
//@Time: 2023/06/20 22:14:13
package main

import (
	"fmt"
	"regexp"
)

/** 正则表达式 */

func main() {
	// 构建一个正则表达式的对象
	reg := regexp.MustCompile(`^[a-z]+\[[0-9]+\]$`)
	// 判断给定的字符串是否符合正则
	fmt.Println("bool:", reg.MatchString("abc[123456]")) // true

	// 从给定的字符串查找符合条件的字符串, -1表示所有
	bytes := reg.FindAll([]byte("efg[456]"), -1)
	fmt.Println("find_all:", string(bytes[0])) // efg[456]
}
