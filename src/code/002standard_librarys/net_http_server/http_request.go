package main

import (
	"fmt"
	"net/http"
	"net/http/httputil"
)

// http request: https://pkg.go.dev/net/http

func httpRequest() {
	request, _ := http.NewRequest(http.MethodGet, "http://www.imooc.com", nil)
	// 设置请求头
	request.Header.Add("User-Agent", "Mozilla/5.0 (iPhone; CPU iPhone OS 13_2_3 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0.3 Mobile/15E148 Safari/604.1")
	// 发起请求
	resp, err := http.DefaultClient.Do(request)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	s, err := httputil.DumpResponse(resp, true)
	if err != nil {
		panic(err)
	}
	fmt.Println("s:\n", s)
}
