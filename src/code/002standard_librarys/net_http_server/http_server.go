package main

import (
	"fmt"
	"io"
	"net/http"
)

// http server

func server() {
	// 1. 注册路由的回调函数
	http.HandleFunc("/", func(rw http.ResponseWriter, r *http.Request) {
		// r 是请求体 = request
		fmt.Println("request: ", r)
		// rw是写给服务端返回的数据 = writer
		_, _ = io.WriteString(rw, "这是/index请求返回的数据!")
	})

	http.HandleFunc("/user", func(rw http.ResponseWriter, r *http.Request) {
		_, _ = io.WriteString(rw, "这是/user请求返回的数据!")
	})

	http.HandleFunc("/goods", func(rw http.ResponseWriter, r *http.Request) {
		_, _ = io.WriteString(rw, "这是/goods请求返回的数据!")
	})

	fmt.Println("http server start...")
	// 2. 监听端口服务
	if err := http.ListenAndServe("127.0.0.1:8080", nil); err != nil {
		fmt.Println("http server failed, err: ", err)
		return
	}
}
