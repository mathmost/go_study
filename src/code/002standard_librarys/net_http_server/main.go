package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
)

// net/http

type JsonRequestBody struct {
	Mobile   string `json:"mobile"`
	Password string `json:"password"`
}

func getRequest() {
	resp, err := http.Get(getUrl)
	if err != nil {
		fmt.Printf("get failed, err: %v\n", err)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("read from resp.Body failed, err: %v\n", err)
		return
	}
	fmt.Println(string(body))
}

// func withParamsGetRequest() {
// 	apiUrl := "http://127.0.0.1:9090/get"
// 	// URL param
// 	data := url.Values{}
// 	data.Set("name", "小王子")
// 	data.Set("age", "18")
// 	u, err := url.ParseRequestURI(apiUrl)
// 	if err != nil {
// 		fmt.Printf("parse url requestUrl failed, err:%v\n", err)
// 	}
// 	u.RawQuery = data.Encode() // URL encode
// 	fmt.Println(u.String())
// 	resp, err := http.Get(u.String())
// 	if err != nil {
// 		fmt.Printf("post failed, err:%v\n", err)
// 		return
// 	}
// 	defer resp.Body.Close()
// 	b, err := ioutil.ReadAll(resp.Body)
// 	if err != nil {
// 		fmt.Printf("get resp failed, err:%v\n", err)
// 		return
// 	}
// 	fmt.Println(string(b))
// }

func postRequest() {
	// urlencoded格式
	data := "mobile=17802156775&password=123456"

	// json格式
	// 第一种方式
	// request := JsonRequestBody{
	// 	Mobile:   "17802156775",
	// 	Password: "123456",
	// }
	// requestBody := new(bytes.Buffer)
	// json.NewEncoder(requestBody).Encode(request)
	// resp, err := http.NewRequest("POST", postUrl, requestBody)

	// 第二种方式
	// data := `{
	// 	"mobile": "17802156775",
	// 	"password": "123456"
	// }`
	// resp, err := http.Post(postUrl, contentType, strings.NewReader(data))

	resp, err := http.Post(postUrl, contentType, strings.NewReader(data))
	if err != nil {
		fmt.Printf("post failed, err:%v\n", err)
		return
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Printf("get resp failed, err:%v\n", err)
		return
	}
	fmt.Println(resp.StatusCode)  // 200
	fmt.Println(resp.Request.URL) // http://www.niuinfo.com/test/user/login?
	fmt.Println(string(body))
}

func main() {
	// 1. get
	getRequest()

	// 2. 带参数的get请求
	// withParamsGetRequest()

	// 3. post
	postRequest()

	// 4. http-server
	server()

	// 5. httpRequest
	httpRequest()
}
