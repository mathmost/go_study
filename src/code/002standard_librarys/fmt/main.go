package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strings"
)

func bufioDemo() {
	reader := bufio.NewReader(os.Stdin) // 从标准输入生成读对象
	fmt.Print("请输入内容: ")
	text, _ := reader.ReadString('\n') // 读到换行结束
	text = strings.TrimSpace(text)
	fmt.Printf("%#v\n", text)
}

func main() {
	// 1. Sprint
	name := "沙河小王子"
	age := 18
	s2 := fmt.Sprintf("name:%s,age:%d", name, age)
	s3 := fmt.Sprintln("沙河小王子")
	fmt.Println(s2, s3) // name:沙河小王子,age:18 沙河小王子

	// 2. Errorf
	e := errors.New("原始错误e")
	w := fmt.Errorf("Wrap发生了一个错误%w", e)
	fmt.Println(w) // Wrap发生了一个错误原始错误e

	// 3. %T打印值的类型
	a := struct{ name string }{
		"小王子",
	}
	fmt.Printf("a值的类型为: %T\n", a) // a值的类型为: struct { name string }

	// 4. %%百分号
	fmt.Printf("100%%\n") // 100%

	// 5. %t布尔型
	b := false
	fmt.Printf("%t\n", b) // false

	// 6. %f %d %s：浮点型、整型、字符串

	// 7. []byte
	s := "小王子"
	//	   %x 每个字节用两字符十六进制数表示(使用a-f)
	fmt.Printf("%x\n", s) // e5b08fe78e8be5ad90
	//     %X 每个字节用两字符十六进制数表示(使用A-F)
	fmt.Printf("%X\n", s) // E5B08FE78E8BE5AD90

	// 8. %p指针: 表示为十六进制，并加上前导的0x
	abc := 10
	fmt.Printf("%p\n", &abc)  // 0xc0000120a0
	fmt.Printf("%#p\n", &abc) // c0000120a0

	// 9. 获取输入
	// var (
	// 	class_number int
	// 	class_name   string
	// 	class_count  int
	// )
	// fmt.Scanln(&class_number, &class_name, &class_count)
	// fmt.Printf("终端获取的结果为: number: %d, name: %s, count: %d\n", class_number, class_name, class_count)

	// 10. bufio.NewReader
	bufioDemo()
}
