package main

import (
	"flag"
	"fmt"
	"os"
	"time"
)

/**
命令行参数
1. flag包支持类型有bool int float64 uint uint64 float string duration
*/

func main() {
	// 1. 传统的获取命令行参数
	fmt.Println("命令行参数:", os.Args)

	// 2. flag.Type(key, value, notice string)
	// 创建一个标志位参数
	name := flag.String("name", "张三", "请输入姓名")
	age := flag.Int("age", 18, "请输入年龄")
	sex := flag.String("sex", "男", "请输入性别")
	married := flag.Bool("married", false, "是否结婚?")
	ct := flag.Duration("ct", time.Second, "结婚多久了?")
	/**
	时间段flag:
	1. 可以传入任意合法的字符串，如“3000ms”, "-1.7h", "2h45m"
	2. 任意的合法单位: "ns", "us"/"ms", "s", "m", "h"
	*/

	// 使用
	flag.Parse()
	/**
	1. go build
	2. ./flag.exe -name=sunwukong -age=500 -sex=男 -married=true -ct=300ms
	3. 或者 /flag.exe -name sunwukong -age=500 --sex=男  -married=true  -ct=300ms
	*/
	// 输出
	// 此时name age sex对应的均为队形类型的指针
	fmt.Println(name, age, sex, married, ct) // 0xc00005c2a0 0xc000016088 0xc00005c2b0 0xc00005c2b8 0xc00005c2c0
	fmt.Println(*name, *age, *sex, *ct)      // sunwukong 500 男 true 3000ms
}
