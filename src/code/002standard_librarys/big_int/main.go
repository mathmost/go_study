//@Author: cyl
//@File: main.go
//@Time: 2023/07/02 21:07:59
package main

import (
	"fmt"
	"math/big"
)

/**
big包
对于较大的整数(超过10的18次方): big.NewInt
对于任意精度的浮点类型: big.NewFloat
对于分数, 比如三方之一: big.NewRat
*/

func main() {
	// 数字: 表示很大的数(如果没有为指数形式的数值指定类型的话，那么go会将其作为float64类型)
	var distance = 24e18
	fmt.Printf("distance: %T, %v\n", distance, distance) // distance: float64, 2.4e+19

	// big.Int: 需要从int64转换而来, 慎用
	lightSpeed := big.NewInt(299792)
	secondsPerDay := big.NewInt(86400)
	fmt.Printf("lightSpeed type: %T, value: %v\n", lightSpeed, lightSpeed)          // *big.Int 299792
	fmt.Printf("secondsPerDay type: %T, value: %v\n", secondsPerDay, secondsPerDay) // *big.Int 86400
	// 如上情况，因为NewInt中需要传递的参数类型为int64，但是如果传递的值超过int64本身的范围后，则会报错
	// 可以先创建一个big.Int类型，再将其设置为string类型，并制定转换的进制
	distanceOver := new(big.Int) // distanceOver.TypeOf = *big.Int
	distanceOver.SetString("2400000000000000000000", 10)
	fmt.Printf("distanceOver type: %T, value: %s\n", distanceOver, distanceOver) // type: *big.Int, value: 24000...
}
