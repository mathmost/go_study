package main

import (
	"fmt"
	"strings"
)

/**
官网文档: https://studygolang.com/pkgdoc strings模块
1. golang统一编码为utf-8, 字母和数字占一个字节, 汉字占3个字节
*/
func main() {
	str := "hello, 我的世界!"
	// 1. 统计字符串长度
	fmt.Println("字符串长度:", len(str)) // 20

	// 2. 字符串遍历
	r := []rune(str)
	for i := 0; i < len(r); i++ {
		fmt.Printf("字符串元素:%c\n", r[i])
	}

	// 3. 替换: Replace(s, old, new string, n int) string
	// 返回将s中前n个不重叠old子串都替换为new的新字符串，如果n<0会替换所有old子串
	fmt.Println(strings.Replace("hellohellphwdjh", "h", "hhh", 2))  // hhhellohhhellphwdjh
	fmt.Println(strings.Replace("hellohellphwdjh", "h", "hhh", -1)) // hhhellohhhellphhhwdjhhh

	// 4. 大写: ToUpper(s string) string
	// 返回将所有字母都转为对应的小写版本的拷贝

	fmt.Println(strings.ToUpper("Gopher")) // GOPHER

	// 5. 小写: ToLower(s string) string
	// 返回将所有字母都转为对应的大写版本的拷贝
	fmt.Println(strings.ToLower("Gopher")) // gopher

	// 6.Contains(s, substr string) bool
	// 判断字符串s是否包含子串substr
	fmt.Println(strings.Contains("seafood", "foo")) // true
	fmt.Println(strings.Contains("seafood", "bar")) // false
	fmt.Println(strings.Contains("seafood", ""))    // true
	fmt.Println(strings.Contains("", ""))           // true

	// 7. EqualFold(s, t string) bool
	// 判断两个utf-8编码字符串（将unicode大写、小写、标题三种格式字符视为相同）是否相同
	fmt.Println(strings.EqualFold("Go", "go")) // true

	// 8. 判断以xxx结尾或开头
	s2 := "2023年5月1号课程笔记.txt"
	if strings.HasPrefix(s2, "2023") {
		fmt.Println("该文本以2023开头...")
	}
	if strings.HasSuffix(s2, "txt") {
		fmt.Println("该文本类型为txt...")
	}

	// 8. 统计个数: Count(s, sep string) int
	// 返回字符串s中有几个不重复的sep子串
	fmt.Println(strings.Count("cheese", "e")) // 3

	// 9. 索引: Index(s, sep string) int
	// 子串sep在字符串s中第一次出现的位置，不存在则返回-1
	fmt.Println(strings.Index("chicken", "e"))   // 5
	fmt.Println(strings.Index("chicken", "ken")) // 4
	fmt.Println(strings.Index("chicken", "dmr")) // -1

	// 10. 去除空格: TrimSpace(s string) string
	// 返回将s前后端所有空白（unicode.IsSpace指定）都去掉的字符串
	fmt.Println(strings.TrimSpace("   hello, my baby!   ")) // hello, my baby!

	/**
	11. 分割: Split(s, sep string) []string
		用去掉s中出现的sep的方式进行分割，会分割到结尾，并返回生成的所有片段组成的切片
		（每一个sep都会进行一次切割，即使两个sep相邻，也会进行两次切割）
		如果sep为空字符，Split会将s切分成每一个unicode码值一个字符串
	*/
	fmt.Printf("%q\n", strings.Split("a,b,c", ","))                        // ["a" "b" "c"]
	fmt.Printf("%q\n", strings.Split("a man a plan a canal panama", "a ")) // ["" "man " "plan " "canal panama"]
	fmt.Printf("%q\n", strings.Split(" xyz ", ""))                         //[" " "x" "y" "z" " "]
	fmt.Printf("%q\n", strings.Split("", "Bernardo O'Higgins"))            // [""]

	// 12. 拼接: Join(a []string, sep string) string
	// 将一系列字符串连接为一个字符串，之间用sep来分隔
	s := []string{"foo", "bar", "baz"}
	fmt.Println(strings.Join(s, ", ")) // foo, bar, baz

	// 13. 重复: Repeat(s string, count int) string
	fmt.Println("ba" + strings.Repeat("na", 2)) // banana

	// 14. 截取(string的切片)
	var strSlice = "helloWorld!"
	fmt.Println("str slice:", strSlice[3:]) // str slice: loWorld!
}
